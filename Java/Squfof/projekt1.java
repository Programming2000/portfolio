/*
 * Primes Squfof
 * 2013 12
 *
 * 
 * Thomas Nilson
 *
*/

/*
 	Integer factorization. Shanks' square forms factorization.
	Time complexity O(N^(1/4)). 

	Test:

	521 * 1123 = 585083

	50021 * 75011 = 3 752 125 231

	500 009 * 1 000 003 = 500 010 500 027

	1 000 003 * 1 000 037 = 1 000 040 000 111

	5 000 011 * 1 000 003 = 5 000 026 000 033

	50 000 017 * 1 000 003 = 50 000 167 000 051
	14 digits
	
	10 000 019 * 15 455 591 = 154 556 203 656 229
	15 digits
	k = 3
	Tid p� 2.1 MHz processor: 0.016 s
	
	87 999 997 * 100 050 001 = 8 804 399 787 849 997
	16 digits
	k = 7
	Tid p� 2.1 MHz processor: 0.063 s
	With testbit(1) for testing perfect square: 0.047 s
	
	499 999 993 * 749 555 539 = 374 777 764 253 111 227
	18 digits
	Tid p� 2.1 MHz processor: 0.078 s
	With testbit(1) for testing perfect square: 0.050 s
	
	http://en.wikipedia.org/wiki/Shanks%27_square_forms_factorization
*/

import java.math.*;
import java.util.*;

// import java.util.Random;

public class projekt1 {

public static void main(String[] args) {

	BigInteger n;
	BigInteger k;
	BigInteger kn;
	BigInteger rootkn;
	BigInteger p0, p1;
	BigInteger q0, q1, q2;
	BigInteger b1;
	BigInteger number;
	BigInteger b0;
	BigInteger pOld;
	BigInteger a, b;

	int digits;
	int counter1;
	int counter2;	
	
	long start, elapsedTimeMs;
	
	Scanner input= new Scanner(System.in);
	System.out.println("Shanks square forms factorization");
	System.out.println("\nNumber n to factor: ");
	
	n = BigInteger.ZERO;
	k = BigInteger.ZERO;
	
	try {
		n = input.nextBigInteger();
	} catch (NumberFormatException ex) {
	}
	
	digits =  numberOfDigits(n);
	System.out.println("\nNumber of digits: " + digits);

	do {
		counter1 = 0;
		counter2 = 0;
		/*
		{1, 3, 5, 7, 11, 3*5, 3*7, 3*11, 5*7, 5*11, 7*11, 
         3*5*7, 3*5*11, 3*7*11, 
         5*7*11, 3*5*7*11 }
		*/
		System.out.println("\nSmall integer k, try 1: ");
		
		try {
			k = input.nextBigInteger();
		} catch (NumberFormatException ex) {
		}
		
		start = System.currentTimeMillis();

		kn = n.multiply(k);
		rootkn = sqrt(kn);

		p0 = rootkn;
		q0 = BigInteger.ONE;
		q1 = kn.subtract(p0.multiply(p0));
		q2 = q1;
	
		// System.out.println("\np0: " + p0 + "   q0: " + q0 + "   q1: " + q1);
		// System.out.println("\n");
		
		do {
			// b1 = (rootkn + p0) / q1;
			b1 = rootkn.add(p0).divide(q1);
			// p1 = b1 * q1 - p0;
			p1 = b1.multiply(q1).subtract(p0);
			// q2 = q0 + b1 * (p0 - p1);
			q2 = p0.subtract(p1).multiply(b1).add(q0);
			
			// System.out.println("\nb: " + b1 + "   p: " + p1 + "   q1: " + q1
			// 		+ "   q2: " + q2);

			q0 = q1;
			q1 = q2;
			// pOld = p0;
			p0 = p1;

			// input.nextLine();

			// number = q2.mod(sqrt(q2));
			
			// Faster test for perfect square.
			// Number is perfect square => number mod 4 = 0 or 1.
			// testbit(1) is false if mod 4 = 0 or 1.
			// http://stackoverflow.com/questions/2685524/check-if-biginteger-is-not-a-perfect-square
			number = BigInteger.ONE;
			if (!q2.testBit(1)) {
				number = q2.mod(sqrt(q2));
			}			  
			
			counter1++;
		} while ((!number.equals(BigInteger.ZERO)) && counter1 < 100000);

		System.out.println("\nPerfect square: " + q2);

		q0 = sqrt(q2);
		b0 = rootkn.subtract(p1).divide(q0);
		p0 = b0.multiply(q0).add(p1);
		q1 = kn.subtract(p0.multiply(p0)).divide(q0);

		// System.out.println("\np0: " + p0 + "   q0: " + q0 + "   q1: " + q1);
		
		do {
			// b1 = (rootkn + p0) / q1;
			// p1 = b1 * q1 - p0;
			// q2 = q0 + b1 * (p0 - p1);
			b1 = rootkn.add(p0).divide(q1);
			p1 = b1.multiply(q1).subtract(p0);
			q2 = p0.subtract(p1).multiply(b1).add(q0);
			
			// System.out.println("\nb: " + b1 + "   p: " + p1 + "   q1: " + q1
			// 		+ "   q2: " + q2);

			q0 = q1;
			q1 = q2;
			pOld = p0;
			p0 = p1;

			// input.nextLine();

			counter2++;
		} while (!p1.equals(pOld) && counter2 < 100000);

		System.out.println("\np: " + p1);

		a = BigInteger.ONE;
		
		if (p1.equals(pOld)) {
			a = n.gcd(p1);
		}
	} while (a.equals(BigInteger.ONE) || a.equals(n));

	b = n.divide(a);
	System.out.println("gcd(" + n + ", " + p1 + ") = " + a);

	elapsedTimeMs = System.currentTimeMillis() - start;
	System.out.println("\nTime for computing: " + (elapsedTimeMs / 1000F) + " s");	
	
	System.out.println("Iterations in first loop: " + counter1);
	System.out.println("Iterations in second loop: " + counter2);
	System.out.println("\n" + a + " * " + b + " = " + n);		
		
}
	  
	public static int numberOfDigits(BigInteger number) {
		int length = 0;

		while (!number.equals(BigInteger.ZERO)) {
			number = number.divide(BigInteger.TEN);
			length ++;
		}

		return length;
	}

	public static BigInteger sqrt(final BigInteger val) {
	    final BigInteger two = BigInteger.valueOf(2);
	    BigInteger a = BigInteger.ONE.shiftLeft(val.bitLength() / 2);
	    BigInteger b;
	    
	    do {
	        b = val.divide(a);
	        a = (a.add(b)).divide(two);
	    } while (a.subtract(b).abs().compareTo(two) >= 0);
	    
	    return a;
	}	
	
	/*
	public static BigInteger sqrt(BigInteger x) {
		final BigInteger two = BigInteger.valueOf(2);
	    BigInteger div = BigInteger.ZERO.setBit(x.bitLength()/2);
	    BigInteger div2 = div;
	    // Loop until we hit the same value twice in a row, or wind
	    // up alternating.
	    for(;;) {
	        BigInteger y = div.add(x.divide(div)).divide(two);
	        
	        if (y.equals(div) || y.equals(div2))
	            return y;
	        
	        div2 = div;
	        div = y;
	    }
	}
	*/
	
	/*
	public static BigInteger sqrt(BigInteger A) {
		final BigInteger two = BigInteger.valueOf(2);
	    BigInteger temp = A.shiftRight(BigInteger.valueOf(A.bitLength()).shiftRight(1).intValue());
	    BigInteger result = temp;
	    
	    do  {
	    	temp = result;
			result = temp.add(A.divide(temp)).divide(two);
	    } while (!temp.equals(result));
	    
	    return result;
	}
	*/
}
