/*
 * Producing primes.
 * 
 * Thomas Nilson
*/

/*
 * Miller-Rabin probalistic primality testing.
 * Each test has a probability of 1/4 to let a non-prime through.
 * For x consecutive tests the probability is 1-1/(4^x) that it is a prime.
 * 
 * http://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
 * http://www.wikihow.com/Check-if-a-Number-Is-Prime
 * http://en.literateprograms.org/Miller-Rabin_primality_test_%28Java%29
 */

import java.math.*;
import java.util.*;

import java.util.Random;

public class projekt1 {

private static final Random rnd = new Random();	
private static final BigInteger ZERO = new BigInteger("0");
private static final BigInteger ONE = new BigInteger("1");
private static final BigInteger TWO = new BigInteger("2");

/*
Number of bits in numbers.
Generates primes with at least the 2^("bits" - 1) bit set giving a
number in the range [2^b-1, 2^(b-1)].
Bits = digits / log(2) = digits / 0.30103
Digits = bits * log(2) = bits * 0.30103
10^a = 2^b <=> log(10^a) = log(2^b) <=> a = b * log(2)
*/
private static final int bits = 48;
// Number of checks for primality.
private static final int loops = 25;
// Number of numbers to produce and store in number[].
private static final int numberOf = 2; 

public static void main(String[] args) {

	BigInteger[] number;
	number = new BigInteger[numberOf];
	int counter = 0;
	int loop = loops;
	
	BigInteger e, f, d;
	BigInteger s, n;
	BigInteger c, z;
	
	BigInteger probprim;

	// Generates primes with at least the 2^("bits" - 1) bit set giving a
	// number in the range [2^b-1, 2^(b-1)].
	do {
		do {
			probprim = new BigInteger(bits, rnd);
		} while (!probprim.testBit(bits - 1));
		
		while(isEven(probprim)) {
			do {
				probprim = new BigInteger(bits, rnd);
			} while (!probprim.testBit(bits - 1));
		}

		if (checkIfPrime(probprim, loop)) {
			
			System.out.println(probprim);
			number[counter] = probprim;
			counter++;
		}
	} while (counter < numberOf);
		
		e = TWO;
		e = (e.pow(16)).add(ONE);
		
		f = (number[0].subtract(ONE)).multiply(number[1].subtract(ONE)).divide(TWO);
		d = inverse(e, f);
		System.out.println("f:       " + f);
		System.out.println("inverse: " + d);
		
		System.out.println("control: " + e.multiply(d).mod(f));
		
		n = number[0].multiply(number[1]);
		s = new BigInteger(n.bitLength() - 1, rnd);
		
		c = exp_mod(s, e, n);
		z = exp_mod(c, d, n);
		
		System.out.println("s: " + s);
		System.out.println("c: " + c);
		System.out.println("z: " + z);
	}
	  

	public static boolean checkIfPrime(BigInteger n, int checks) {
		BigInteger a=new BigInteger(n.bitLength()-1, rnd);
	    
	    for (int i = 0; i < checks; i++) {
	    	while (a.compareTo(ZERO) <= 0 || a.compareTo(n) >= 0)
	    		a = new BigInteger(n.bitLength()-1, rnd);
	    	if (!witness(a,n)) { 
	    		return false; 
	    	}
	    }
	    
	    return true;
	}

	public static boolean witness(BigInteger a, BigInteger n) {
		  
		BigInteger oneless = n.subtract(ONE); 
		  
		BigInteger d = oneless;
		int s = d.getLowestSetBit(); 
		d = d.shiftRight(s);
		  
		// Tidigare: BigInteger asq_modn = a.modPow(oneless, n); 
		BigInteger asq_modn = a.modPow(d, n);
		
		if (asq_modn.equals(ONE))
			return true;
		    
		for (int i = 0; i < s - 1; i++) {
			if (asq_modn.equals(oneless))
				return true;
		        
			asq_modn = asq_modn.multiply(asq_modn).mod(n);
		}
		  
		if (asq_modn.equals(oneless))
			return true;
		    
		return false;
	}   
	  
	public static boolean isEven(BigInteger i){
		return !i.testBit(0);
	}
	  
	public static BigInteger exp_mod(BigInteger a, BigInteger x, BigInteger N) {
		// Assume a >= 0, x >= 0, N > 1
		if (a.compareTo(ZERO) == 0) {
			return ZERO;
		}
		  
		BigInteger res = ONE;
		BigInteger y = a;
		  
		while (x.compareTo(ZERO) > 0) {
			if (!isEven(x)) {
				res = (res.multiply(y)).mod(N);
			}
			  
			y = (y.multiply(y)).mod(N);
			x = x.divide(TWO);
		}
		  
		return res;
	}
	  
	public static BigInteger inverse(BigInteger a, BigInteger m) {
		BigInteger d1 = m;
		BigInteger v1 = ZERO;
		BigInteger v2 = ONE;
		BigInteger d2 = a;
		BigInteger q, t2, t3;
		BigInteger v, d;
		  
		while (d2.compareTo(ZERO) != 0) {
			q = d1.divide(d2);
			t2 = v1.subtract(q.multiply(v2));
			t3 = d1.subtract(q.multiply(d2));
			v1 = v2;
			d1 = d2;
			v2 = t2;
			d2 = t3;
		}
		  
		v = v1;
		d = d1;
		  
		if (d.compareTo(ONE) == 0) { 
			while (v.compareTo(ZERO) == -1) {
				v = v.add(m);
			}
			return v;
		} else
			return ZERO;
	}
}
