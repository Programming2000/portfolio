/**
 * Uppg3bver2.java
 * Inlämningsuppgift 3
 * Thomas Nilson
 */

import javax.swing.*;

/**
 * Skapar ett nytt fönster och lägger till korten.
 */
public class Uppg3bver2 extends JFrame {
	public Uppg3bver2() {
		setTitle("Uppgift 3");
		add(new Playfield());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100,700,500);
		setVisible(true);
	}

    public static void main(String[] args) {
		new Uppg3bver2();
    }
}