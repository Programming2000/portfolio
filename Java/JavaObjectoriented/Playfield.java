/**
 * Playfield.java
 * Inl�mningsuppgift 3
 * Thomas Nilson
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.Component;

/**
 * L�ser in och placerar korten i f�nstret.
 * Hanterar klickning p� kort och f�rflyttning av kort.
 */
class Playfield extends JLayeredPane implements MouseListener, MouseMotionListener {
	// En kortlek med 52 kort.
	private Card[] cards = new Card[52];
	// Tecken som s�tts samman till filnamn.
	private final char[] n1 = {'d','h','c','s'};
	private final String[] n2 = {"1","2","3","4","5","6","7","8","9","10","j","q","k"};
	// Bilden av baksidan.
	private ImageIcon backside;
	// Om en f�rflyttning inte p�b�rjats.
	private boolean check = true;
	// Muspekarens absoluta l�ge vid kortets f�rra l�ge.
	private int x_saved, y_saved;

	/**
	 * H�mtar kortbilder och l�gger till dem till JPanels.
	 * S�tter namnet i ordningsf�ljd s� att de kan hittas vid en mouseevent.
	 * S�tter position och att bildsidan visas upp�t.
	 */
 	public Playfield() {
		int count = 0;
		String path;
		JLabel label;
		ImageIcon pict;

		setLayout(null);

		for (int i = 0; i < 4; i++) {
			for(int j = 0; j < 13; j++) {
				path = "img/" + n1[i] + n2[j] + ".gif";
				pict = new ImageIcon(path);
				label = new JLabel(pict);
				// Referenserna till bild och label skickas till ett nytt kort.
				cards[count] = new Card(label, pict);
				label.setName(Integer.toString(count));
				label.setBounds(10 + j * 40, 10 + i * 40, 71, 96);
				label.addMouseListener(this);
				label.addMouseMotionListener(this);
				add(label);
				count++;
			}
		}

		backside = new ImageIcon("img/b1fv.gif");
	}

	/**
	 * Om musen klickas ska kortet v�ndas.
	 * @param e
	 */
	public void mouseClicked(MouseEvent e) {
		// "JLabel label = (JLabel)e.getComponent();" hade gett labeln direkt
		Component comp = e.getComponent();
		// Kortets namn �r dess index.
		int index = Integer.valueOf(comp.getName());
		// Kortets JLabel.
		JLabel label = cards[index].getLabel();

		// Placerar kortet �verst.
		moveToFront(label);

		// Byter bild p� kortet.
		if (cards[index].getFace()) {
			label.setIcon(backside);
		} else {
			label.setIcon(cards[index].getPicture());
		}

		// �ndrar status p� vilken sida som visas upp�t.
		cards[index].changeFace();
	}

	/**
	 * Flyttning av kortet.
	 * @param e
	 */
	public void mouseDragged (MouseEvent e) {
		// "JLabel label = (JLabel)e.getComponent();" hade gett labeln direkt
		Component comp = e.getComponent();
		// Kortets namn �r dess index.
		int index = Integer.valueOf(comp.getName());
		// Kortets JLabel.
		JLabel label = cards[index].getLabel();

		// Om f�rflyttningen precis har p�b�rjats.
		// Kortet placeras �verst.
		// Muspekarens absoluta position sparas.
		if (check == true) {
			moveToFront(comp);
			check = false;
			Point pos = MouseInfo.getPointerInfo().getLocation();
			x_saved = (int)pos.getX();
			y_saved = (int)pos.getY();
		}

		Point pos2 = MouseInfo.getPointerInfo().getLocation();
		// Kortet f�rflyttas.
		label.setLocation(label.getX() + (int)pos2.getX() - x_saved, label.getY() + (int)pos2.getY() - y_saved);
		// Muspekarens absoluta position sparas.
		x_saved = (int)pos2.getX();
		y_saved = (int)pos2.getY();
	}

	public void mouseEntered (MouseEvent e) { }
    public void mouseExited (MouseEvent e) { }
    public void mousePressed (MouseEvent e) { }
    public void mouseReleased (MouseEvent e) {
    	check = true;
    }

	public void mouseMoved (MouseEvent e) {	}

	@Override
	protected void paintComponent(Graphics g) {
		// Anropas n�r en JLabel ritas om eller repaint() anropas.
	}
}
