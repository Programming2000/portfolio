/**
 * Card.java
 * Inlämningsuppgift 3
 * Thomas Nilson
 */

import javax.swing.*;

public class Card {
	private JLabel label;
	private ImageIcon picture;
	boolean face;

    public Card(JLabel label, ImageIcon picture) {
    	this.label = label;
    	this.picture = picture;
    	face = true;
    }

	public ImageIcon getPicture() {
		return picture;
	}

	public JLabel getLabel() {
		return label;
	}

	public boolean getFace() {
		return face;
	}

	public void changeFace() {
		face = !face;
	}
}