/* 
   Game of Life
   Run in windows.
   Assignment 4
   Thomas Nilson
   gda08tni@student.lu.se
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <windows.h>
#include <conio.h>
#include <ctime>
#include <sstream>

using namespace std;

const int spaceX = 42;
const int spaceY = 22;
const int dx[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
const int dy[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
char symbol[2] = {'0', '1'};

void gotoXY(int x, int y);
void getCursorXY(int &x, int&y);
void meny1();
void removeMeny1();
void choiceLine();
int enterLine(string text, int now, int low, int high);
char enterSymbol(string text, char symb);
void show(int **c);
void clear(int **c);
void update(int **c, int **d, bool show);
void randomPattern(int **c, int probability);
void edit(int **c);
void add(int **c, int x, int y, int x2, int y2);
void rowWithTen(int **c, int x, int y);
void glider(int **c, int x, int y);
void rPentomino(int **c, int x, int y);
void pulsar(int **c, int x, int y);
void orthogonalSpaceship(int **c, int x, int y);
void queenBeeShuttle(int **c, int x, int y);

int main(int argc, char *argv[]) {
  int **a;
  int **b;
  int **e;
  char ch = '\0';
  int posX = 9;
  int answer;
  int waitingTime = 100;
  
  cout << endl << " The program is for windows." << endl;
  Sleep(2000);
  
  // Randomize
  srand(time(0));
  
  // *a[spaceX]
  a = new int *[spaceX];
  b = new int *[spaceX];

  // a[spaceX][spaceY]
  for (int i = 0; i < spaceX; i++) {
    a[i] = new int [spaceY];
    b[i] = new int [spaceY];
  }        
  
  // Clear grid and gridbuffert. Show grid, menu and choiceline.
  clear(a);
  clear(b);
  show(a);
  meny1();
  choiceLine();
  
  // Main loop
  do {
    gotoXY(posX,22);
    ch = _getch();
    
    switch (ch) {
      // "1: Step one generation"
      case '1':
        update(a, b, true);
        // Swap a[][] and b[][]
        e = a;
        a = b;
        b = e;
        break;
      // "2: Enter number of generations to go [0, 1000]:"
      case '2':
        answer = enterLine("Number of generations", -1, 0, 1000);
        for (int k = 0; k < answer; k++) {
          Sleep(waitingTime);
          update(a, b, true);
          // Swap a[][] and b[][]
          e = a;
          a = b;
          b = e;
        }        
        choiceLine();
        break;
      // "3: Enter delay between generations (waitingTime)[0, 5000]:"
      case '3':
        waitingTime = enterLine("Delay", waitingTime, 0 , 5000);
        choiceLine();
        break;
      // "4: Fast forward this many generations"
      case '4':
        answer = enterLine("Number of generations to ff", -1, 0, 10000);
        for (int k = 0; k < answer; k++) {
          update(a, b, false);
          // Swap a[][] and b[][]
          e = a;
          a = b;
          b = e;
        }
        show(a);
        choiceLine();
        break;
      // "5: Random pattern"
      case '5':
        answer = enterLine("Probability for a cell", -1, 0, 1000);
        randomPattern(a, answer);
        show(a);
        choiceLine();
        break;
      // "6: Clear grid"
      case '6':
        clear(a);
        show(a);
        break;
      // "7: Edit manually with editor"
      case '7':
        removeMeny1();
        edit(a);
        meny1();
        break;
      // "8: Enter symbol for a cell"
      case '8':
        symbol[1] = enterSymbol("Symbol for a cell", symbol[1]);
        show(a);
        choiceLine();
        break;
      // "9: Enter symbol for a void"
      case '9':
        symbol[0] = enterSymbol("Symbol for a void", symbol[0]);
        show(a);
        choiceLine();
        break;
                
      Sleep(100);
    }
    // "0: Exit program"
  } while (ch != '0');

  // Delete in reverse order from the creation.
  // => *a[spaceX]
  for (int j = 0; j < spaceX; j++) {
    delete[] a[j];
    delete[] b[j];    
  }   
  
  // Delete arrays that are left.
  delete[] a;
  delete[] b;  
                       
  cout << endl << endl;
  // "pause>nul" give no text.
  system("pause");
  return EXIT_SUCCESS;
}

/*
Place cursor at (x, y) on the screen. (0, 0) at the top left of the screen.
@param x Positive values goes to the right
@param y Positive values goes down
*/
void gotoXY(int x, int y) {
  //Initialize the coordinates
  COORD coord = {x, y};
  
  //Set the position
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

/*
Gets the cursors position on the screen.
@param x X-axis
@param y Y-axis
*/
void getCursorXY(int &x, int &y) {
  CONSOLE_SCREEN_BUFFER_INFO csbi;

  if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
    x = csbi.dwCursorPosition.X;
    y = csbi.dwCursorPosition.Y;
  }
}

/*
Shows The Supermenu.
*/
void meny1() {
  gotoXY(43, 5);
  cout << "1: Step one generation";
  gotoXY(43, 6);
  cout << "2: Enter number of generations to go";
  gotoXY(43, 7);
  cout << "3: Enter delay between generations";
  gotoXY(43, 8);
  cout << "4: Fast forward this many generations";
  gotoXY(43, 9);
  cout << "5: Random pattern";
  gotoXY(43, 10);
  cout << "6: Clear grid";
  gotoXY(43, 11);
  cout << "7: Edit manually with editor";
  gotoXY(43, 12);
  cout << "8: Enter symbol for a cell";
  gotoXY(43, 13);
  cout << "9: Enter symbol for a void"; 
  gotoXY(43, 14);
  cout << "0: Exit program";
}

/*
Removes text from the supermenu.
*/
void removeMeny1() {
  for (int i = 5; i < 15; i++) {
    gotoXY(43, i);
    cout << "                                     ";
  } 
}

/*
Shows textline with "Choice: " at the bottom of the screen.
*/
void choiceLine() {
  gotoXY(1,22);
  cout << "Choice: ";
}

/*
Questionline at the bottom of the screen.
text + "(" + now + ")" + "[" + low + ", " + high + "]: "
If now != -1 so show "(" + now + ")"
@param text Text to show
@param now  The current value
@param low  Lower bound
@param high Higher bound
@return The given answer
*/
int enterLine(string text, int now, int low, int high) {
  // Default value
  int newChoice = low;
  int answer;
  string answerString;
  
  gotoXY(1,22);
  cout << "       ";
  
  // New default value: current value
  if (now != -1)
    newChoice = now;
  
  do {
    answerString = "";
    gotoXY(1,22);
    cout << text;
  
    if (now != -1)
      cout << " (now: " << now << ")";

    cout << " [" << low << ", " << high << "]: ";
    getline(cin, answerString);
    // For changing a string to an int.
    stringstream ss(answerString);
    // Tries to make the string an int.
    if ((ss >> answer).fail() || !(ss >> ws).eof()) {
      answer = newChoice;
    }
  } while (answer < low || answer > high);
  
  newChoice = answer;
  gotoXY(1, 22);
  // Delete questionline.
  for (int i = 0; i < 50; i++) {
    cout << " ";
  } 
  
  return newChoice;
}

/*
New symbol for a cell or a void.
@param text Text to show
@param symb Current symbol
@return New symbol
*/
char enterSymbol(string text, char symb) {
  char newChoice = symb;
  string answer = "";

  gotoXY(1,22);
  cout << "       ";

  do {
    gotoXY(1,22);
    cout << text << " (now: " << symb << "): ";
    getline(cin, answer);
  } while (answer == "");  
  
  newChoice = answer.at(0);
  
  gotoXY(1,22);
  cout << "                                        ";
  
  return newChoice;
}

/*
Show the whole grid.
@param c
*/
void show(int **c) {
  gotoXY(1,1);   
    
  for (int i = 1; i < (spaceY - 1); i++) {
    for (int j = 1; j < (spaceX - 1); j++) {
      if (c[j][i] == 0)
        cout << symbol[0];
      else
        cout << symbol[1];
    }
    
    cout << endl << " ";
  }          
}

/*
Clear the grid.
@param c
*/
void clear(int **c) {
  for (int i = 0; i < spaceX; i++) {
    for (int j = 0; j < spaceY; j++) {
      c[i][j] = 0;
    }
  }
}

/*
Updates according to Game of Life rules and changes graphics.
@param c    Old grid
@param d    New grid
@param show Show a change graphically or not
*/
void update(int **c, int **d, bool show) {
  int counter;
  int now, nextNow;
  
  for (int i = 1; i < (spaceY - 1); i++) {
    for (int j = 1; j < (spaceX - 1); j++) {
      now = c[j][i];
      counter = 0;
      
      // Counts the number of neighbours to a cell.
      // Alternative to if: counter += c[j + dx[k]][i + dy[k]]
      for (int k = 0; k < 8; k++) {
        if (c[j + dx[k]][i + dy[k]] == 1)
          counter++;
      }
      
      /*
      A dead cell with exactly 3 neighbours becomes a live cell.
      A live cell with 2 or 3 live neighbours stays alive.
      In all other cases a cell stays dead or dies.
      */
      if ((now == 0 && counter == 3) || (now == 1 && counter == 2)
        || (now == 1 && counter == 3)) {
        d[j][i] = 1;
        nextNow = 1;
      } else {
        d[j][i] = 0;
        nextNow = 0;
      }
      
      // A change is shown if show is true.
      if (show && (nextNow != now)) {
        gotoXY(j,i);
        cout << symbol[nextNow];
      }
    }
  }
}

/*
Make a random pattern in the grid.
@param c
@param probability Value 0-1000 corresponds to 0.000-1.000
*/
void randomPattern(int **c, int probability) {
  for (int i = 1; i < (spaceY - 1); i++) {
    for (int j = 1; j < (spaceX - 1); j++) {
      if ((rand() % 1000) < probability) { 
        c[j][i] = 1;
      } else {
        c[j][i] = 0;
      }
    }
  }
}

/*
Editing the grid with the arrow keys and space. Functionality to add interesting
patterns.
@param c
*/
void edit(int **c) {
  char ch = '\0';
  int x = 20;
  int y = 10;
  
  gotoXY(43, 5);
  cout << "Arrows: Move around";
  gotoXY(43, 6);
  cout << "Space:  Change";
  gotoXY(43, 7);
  cout << "1:      Add row with 10 cells";
  gotoXY(43, 8);
  cout << "2:      Add the Glider";
  gotoXY(43, 9);
  cout << "3:      Add the R-pentomino";
  gotoXY(43, 10);
  cout << "4:      Add the Pulsar";  
  gotoXY(43, 11);
  cout << "5:      Add the Orthogonal spaceship";
  gotoXY(43, 12);
  cout << "6:      Add the Queen Bee Shuttle";  
  gotoXY(43, 13);
  cout << "Enter:  Finished";
  
  do {
    gotoXY(x, y);
    cout << symbol[c[x][y]];
    gotoXY(x, y);
    Sleep(50);
      
    ch = _getch();
    
    switch (ch) {
      // up
      case 72:
        if (y > 0)
          y--;
        break;
      // left
      case 75:
        if (x > 0)
          x--;
        break;
      // right
      case 77:
        if (x < 40)
          x++;
        break;
      // down
      case 80:
        if (y < 20)
          y++;
        break;
      // space => change
      case 32:
        if (c[x][y] == 0)
          c[x][y] = 1;
        else
          c[x][y] = 0;
        break;
      // "1:      Add row with 10 cells"
      case '1':
        rowWithTen(c, x, y);
        break;
      // "2:      Add the Glider"
      case '2':
        glider(c, x, y);
        break;
      // "3:      Add the R-pentomino"
      case '3':
        rPentomino(c, x, y);
        break;
      // "4:      Add the Pulsar"
      case '4':
        pulsar(c, x, y);
        break;
      // "5:      Add the Orthogonal spaceship"
      case '5':
        orthogonalSpaceship(c, x, y);
        break;
      // "6:      Add the Queen Bee Shuttle"
      case '6':
        queenBeeShuttle(c, x, y);
        break;
                
      Sleep(50);
    }
    // enter => finished
  } while (ch != 13);
  
  for (int i = 5; i < 14; i++) {
    gotoXY(43, i);
    cout << "                                    ";
  }
}

/*
Checks that the cell is within the grid. If so adds it and shows it on screen.
@param c
@param x  Startingpoint on x-axis
@param y  Startingpoint on y-axis
@param x2 Relative to x
@param y2 Relative to y
*/
void add(int **c, int x, int y, int x2, int y2) {
  int x3, y3;
  
  x3 = x + x2;
  y3 = y + y2;
  
  if ((x3 >= 1 && x3 <= (spaceX - 2)) && (y3 >= 1 && y3 <= (spaceY - 2))) {
    c[x3][y3] = 1;
    gotoXY(x3, y3);
    cout << symbol[1];
  }
}

/*
Adds a row with ten cells.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void rowWithTen(int **c, int x, int y) {
  for (int i = 0; i < 10; i++) {
      add(c, x, y, i, 0);
  }
}

/*
Adds the Glider pattern.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void glider(int **c, int x, int y) {
  add(c, x, y, 1, 0);
  
  add(c, x, y, 1, 1);
  add(c, x, y, 2, 1);
  
  add(c, x, y, 2, 2);
  add(c, x, y, 0, 2);  
}

/*
Adds the R-pentomino pattern.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void rPentomino(int **c, int x, int y) {
  add(c, x, y, 1, 0);
  add(c, x, y, 2, 0);
  
  add(c, x, y, 0, 1);
  add(c, x, y, 1, 1);
  
  add(c, x, y, 1, 2);  
}

/*
Adds the Pulsar pattern.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void pulsar(int **c, int x, int y) {
  add(c, x, y, 1, 0);
  
  add(c, x, y, 0, 1);
  add(c, x, y, 1, 1);
  add(c, x, y, 2, 1);

  add(c, x, y, 0, 2);
  add(c, x, y, 2, 2);

  add(c, x, y, 0, 3);
  add(c, x, y, 1, 3);
  add(c, x, y, 2, 3);
  
  add(c, x, y, 1, 4);
}

/*
Adds The Orthogonal Spaceship pattern.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void orthogonalSpaceship(int **c, int x, int y) {
  add(c, x, y, 0, 0);
  add(c, x, y, 3, 0);
  
  add(c, x, y, 4, 1);
  
  add(c, x, y, 0, 2);
  add(c, x, y, 4, 2);
  
  add(c, x, y, 1, 3);
  add(c, x, y, 2, 3);
  add(c, x, y, 3, 3);
  add(c, x, y, 4, 3);    
}

/*
Adds The Queen Bee Shuttle pattern.
@param c
@param x Startingpoint on x-axis
@param y Startingpoint on y-axis
*/
void queenBeeShuttle(int **c, int x, int y) {
  add(c, x, y, 0, 0);
  add(c, x, y, 1, 0);
  
  add(c, x, y, 2, 1);
  
  add(c, x, y, 3, 2);
  
  add(c, x, y, 3, 3);
  
  add(c, x, y, 3, 4);

  add(c, x, y, 2, 5);
  
  add(c, x, y, 1, 6);
  add(c, x, y, 0, 6);
}
