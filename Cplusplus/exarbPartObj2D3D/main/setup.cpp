/* 
   setup.cpp
   
   Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>
#include <cmath>

#include "data.h"
#include "setup.h"

using namespace std;

Setup::Setup(Data *data) {
  d = data;
}

// Sets local data to copy data and to point to data from class Data.
void Setup::updateInData() {
  dimension = d->dimension;
  dots = d->dots;
  protons = d->protons;
  x1 = d->x1;
  y1 = d->y1;
  z1 = d->z1;
  vx1 = d->vx1;
  vy1 = d->vy1;
  vz1 = d->vz1;
  xs = d->xs;
  ys = d->ys;
  zs = d->zs;
}

void Setup::latticeStart() {
  if (dimension == 2) {
	// latticeQuadratic();
	latticeQuadraticWithDeplacement();
  } else {
    // latticeCubic();
	latticeCubicWithDeplacement();
  }
}

/*
Makes a lattice with number of particles equal to dots.

The margin to the sides, at -0.5 and 0.5, is the same as the spacing between
particles.
*/
void Setup::latticeQuadratic() {
  double side2;
  double spacing;
  double i, j;
  int count;
  double xt, yt, zt;

  side2 = ceil(sqrt((double)dots)) + 1.0;
  spacing = 1.0 / side2;
  count = 0;

  for (i = 1.0; i < side2; i++) {
    for (j = 1.0; j < side2; j++) {
      xt = spacing * j - 0.5;
      yt = spacing * i - 0.5;

	  if (((protons) > 0) && ((abs(xt) < 1.0e-7) && abs(yt) < (1.0e-7))) {
	    xt += spacing / 2.0;
	  }

      zt = 0;

	  x1[count] = xt;
	  y1[count] = yt;
	  z1[count] = zt;
	  xs[count] = xt;
	  ys[count] = yt;
	  zs[count] = zt;

      count++;

	  if (count == dots) {
	    break;
	  }
    }
	  
	if (count == dots) {
	  break;
	}
  }
}

/*
Makes a lattice with number of particles equal to dots.

The margin to the sides, at -0.5 and 0.5, is the same as the spacing between
particles.
*/
void Setup::latticeQuadraticWithDeplacement() {
  double side2;
  double spacing;
  double deplacement1, deplacement2;
  double i, j;
  int count;
  double xt, yt, zt;

  side2 = ceil(sqrt((double)dots)) + 1.0;
  spacing = 1.0 / side2;
  // +/-spacing/4.0
  // deplacement1 = spacing / 2.0;
  // +/-spacing*3.0/8.0
  deplacement1 = spacing * 3.0 / 4.0;
  deplacement2 = deplacement1 / 2.0;
  count = 0;
  
  for (i = 1.0; i < side2; i++) {
    for (j = 1.0; j < side2; j++) {
      xt = spacing * j - 0.5 + randomNumberDouble(deplacement1) - deplacement2;
      yt = spacing * i - 0.5 + randomNumberDouble(deplacement1) - deplacement2;

      zt = 0;

	  x1[count] = xt;
	  y1[count] = yt;
	  z1[count] = zt;
	  xs[count] = xt;
	  ys[count] = yt;
	  zs[count] = zt;

      count++;

	  if (count == dots) {
	    break;
	  }
    }
	  
	if (count == dots) {
	  break;
	}
  }
}

/*
Makes a lattice with number of particles equal to dots.
The integer part of the cube root of the number of particles gives the side
length of the layers. The rest of the particles
(dots mod (ceiling(cube root(dots))^2)) is added to the last layer.
The margin to the sides, at -0.5 and 0.5, is the same as the spacing between
particles.
*/
void Setup::latticeCubic() {
  double rdots = (double)dots;
  double side, side2;
  double loop1;
  double spacing;
  double i, j, layer;
  int count;
  double xt, yt, zt;

  side = pow(rdots, 1.0 / 3.0);
  side2 = ceil(side) + 1.0;
  spacing = 1.0 / side2;
  loop1 = floor(rdots / ((side2 - 1.0) * (side2 - 1.0))) + 1.0;
  count = 0;
  
  for (layer = 1.0; layer < loop1; layer++) {
    for (i = 1.0; i < side2; i++) {
      for (j = 1.0; j < side2; j++) {
        xt = spacing * j - 0.5;
        yt = spacing * i - 0.5;
        zt = 0.5 - spacing * layer;

		if (((protons) > 0) && ((abs(xt) < 1.0e-7) && abs(yt) < (1.0e-7)
			&& abs(zt) < (1.0e-7)))
		{
		  xt += spacing / 2.0;
		}

		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;

        count++;

		if (count == dots) {
		  break;
		}
      }

	  if (count == dots) {
	    break;
	  }
    }
	
	if (count == dots) {
	  break;
	}
  }

  i = 1.0;
  j = 1.0;

  while (count < dots) {
    xt = spacing * j - 0.5;
    yt = spacing * i - 0.5;
    zt = 0.5 - spacing * loop1;
 	x1[count] = xt;
	y1[count] = yt;
	z1[count] = zt;
	xs[count] = xt;
	ys[count] = yt;
	zs[count] = zt;
    j++;
    
    if (j == side2) {
      j = 1.0;
      i++;
      
      if (i == side2) {
        i = 1.0;
      }
    }
    
    count++;
  }

/*
  for (int i = 0; i < dots; i++) {
    for (int j = 0; j < dots; j++) {
      if ((i !=j ) && (x1[i] == x1[j]) && (y1[i] == y1[j]) && (z1[i] == z1[j])) {
        cout << endl << "!: " << i << "   " << j << "  " << x1[i] << "  " << y1[i] << "  " << z1[i];
	  }
	}
  }
*/

}

void Setup::latticeCubicWithDeplacement() {
  double rdots = (double)dots;
  double side, side2;
  double loop1;
  double spacing;
  double depl1, depl2;
  double i, j, layer;
  int count;
  double xt, yt, zt;

  side = pow(rdots, 1.0 / 3.0);
  side2 = ceil(side) + 1.0;
  spacing = 1.0 / side2;

  // +/-spacing/4.0
  // depl1 = spacing / 2.0;
  // +/-spacing*3.0/8.0
  depl1 = spacing * 3.0 / 4.0;
  depl2 = depl1 / 2.0;

  loop1 = floor(rdots / ((side2 - 1.0) * (side2 - 1.0))) + 1.0;
  count = 0;
  
  for (layer = 1.0; layer < loop1; layer++) {
    for (i = 1.0; i < side2; i++) {
      for (j = 1.0; j < side2; j++) {
        xt = spacing * j - 0.5 + randomNumberDouble(depl1) - depl2;
        yt = spacing * i - 0.5 + randomNumberDouble(depl1) - depl2;
        zt = 0.5 - spacing * layer + randomNumberDouble(depl1) - depl2;

		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;

        count++;

		if (count == dots) {
		  break;
		}
      }

	  if (count == dots) {
	    break;
	  }
    }
	
	if (count == dots) {
	  break;
	}
  }

  i = 1.0;
  j = 1.0;

  while (count < dots) {
    xt = spacing * j - 0.5;
    yt = spacing * i - 0.5;
    zt = 0.5 - spacing * loop1;
 	x1[count] = xt;
	y1[count] = yt;
	z1[count] = zt;
	xs[count] = xt;
	ys[count] = yt;
	zs[count] = zt;
    j++;
    
    if (j == side2) {
      j = 1.0;
      i++;
      
      if (i == side2) {
        i = 1.0;
      }
    }
    
    count++;
  }

/*
  for (int i = 0; i < dots; i++) {
    for (int j = 0; j < dots; j++) {
      if ((i !=j ) && (x1[i] == x1[j]) && (y1[i] == y1[j]) && (z1[i] == z1[j])) {
        cout << endl << "!: " << i << "   " << j << "  " << x1[i] << "  " << y1[i] << "  " << z1[i];
	  }
	}
  }
*/

}

// Body centered cubic lattice "latticeBcc".
// A particle in all the corners and one in the middle of the cube.
//
// Use with dots=10*10*10+10*10*10=2000 and with direct making of
// histogram without iterations.
// Shows the reference Wigner crystal radial distribution.
// http://en.wikipedia.org/wiki/Body-centered_cubic
void Setup::latticeBcc1a() {
  double layerSpacing;
  double i, j, layer;
  int count;
  double xt, yt, zt;

  layerSpacing = 0.05;
  count = 0;
  
  for (layer = -0.5; layer < 0.45; layer += 0.1) {
    for (i = -0.5; i < 0.5; i += 0.1) {
      for (j = -0.5; j < 0.5; j += 0.1) {
		xt = j;
		yt = i;
		zt = layer;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }

	for (i = -0.45; i < 0.5; i += 0.1) {
      for (j = -0.45; j < 0.5; j += 0.1) {
		xt = j;
		yt = i;
		zt = layer + layerSpacing;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }
  }

  cout << endl << "count: " << count;
}

// Body centered cubic lattice "latticeBcc".
// A particle in all the corners and one in the middle of the cube.
//
// Use with dots=250 and with direct making of
// histogram without iterations.
// Shows the reference Wigner crystal radial distribution.
// http://en.wikipedia.org/wiki/Body-centered_cubic
void Setup::latticeBcc1b() {
  double layerSpacing;
  double i, j, layer;
  int count;
  double xt, yt, zt;

  layerSpacing = 0.1;
  count = 0;
  
  for (layer = -0.5; layer < 0.45; layer += 0.2) {
    for (i = -0.5; i < 0.5; i += 0.2) {
      for (j = -0.5; j < 0.5; j += 0.2) {
		xt = j;
		yt = i;
		zt = layer;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }

	for (i = -0.45; i < 0.5; i += 0.2) {
      for (j = -0.45; j < 0.5; j += 0.2) {
		xt = j;
		yt = i;
		zt = layer + layerSpacing;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }
  }

  cout << endl << "count: " << count;
}

// Body centered cubic lattice "latticeBcc2".
// A particle in the middle of the sides and one in the middle of the cube.
//
// Use with dots=4000 and with direct making of
// histogram without iterations.
// Shows the reference Wigner crystal radial distribution.
// http://en.wikipedia.org/wiki/Body-centered_cubic
void Setup::latticeBcc2() {
  double layerSpacing;
  double i, j, layer;
  int count;
  double xt, yt, zt;

  layerSpacing = 0.05;
  count = 0;
  
  for (layer = -0.5; layer < 0.5; layer += 0.1) {
    for (i = -0.5; i < 0.5; i += 0.1) {
      for (j = -0.50; j < 0.5; j += 0.1) {
		xt = j + 0.05;
		yt = i;
		zt = layer;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }

	  for (j = -0.50; j < 0.5; j += 0.1) {
		xt = j;
		yt = i + 0.05;
		zt = layer;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }

	for (i = -0.50; i < 0.5; i += 0.1) {
      for (j = -0.50; j < 0.5; j += 0.1) {
		xt = j;
		yt = i;
		zt = layer + layerSpacing;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }

      for (j = -0.45; j < 0.5; j += 0.1) {
		xt = j + 0.05;
		yt = i + 0.05;
		zt = layer + layerSpacing;
		x1[count] = xt;
		y1[count] = yt;
		z1[count] = zt;
		xs[count] = xt;
		ys[count] = yt;
		zs[count] = zt;
		count++;
      }
    }
  }

  cout << endl << "count: " << count;
}

// Returns a random double in the interval [0, to].
double Setup::randomNumberDouble(double to) {
  return (double)rand() / (double)RAND_MAX * to;
}

void Setup::randomEvenDistr(double margin) {
  double range = 1.0 - margin * 2;
  double adjust = range / 2;
  double xt, yt, zt;

  for (int i = 0; i < dots; i++) {
    xt = randomNumberDouble(range) - adjust;
    yt = randomNumberDouble(range) - adjust;
    zt = randomNumberDouble(range) - adjust;
	x1[i] = xt;
	y1[i] = yt;
	z1[i] = zt;
	xs[i] = xt;
	ys[i] = yt;
	zs[i] = zt;
  }
}

/*
vx = [-maxComponent, maxComponent] and the same for vy and vz.
*/
void Setup::randomVelocities(double maxComponent) {
  double range = 2 * maxComponent;
  double vxt, vyt, vzt;
  double sumvx, sumvy, sumvz;
  double n = (double)dots;

  sumvx = 0;
  sumvy = 0;
  sumvz = 0;

  for (int i = 0; i < dots; i++) {
    vxt = randomNumberDouble(range) - maxComponent;
    vyt = randomNumberDouble(range) - maxComponent;
    vzt = randomNumberDouble(range) - maxComponent;
	sumvx += vxt;
	sumvy += vyt;
	sumvz += vzt;
	vx1[i] = vxt;
	vy1[i] = vyt;
	vz1[i] = vzt;
  }

  sumvx = sumvx / n;
  sumvy = sumvy / n;
  sumvz = sumvz / n;

  for (int i = 0; i < dots; i++) {
    vx1[i] -= sumvx;
    vy1[i] -= sumvy;
    vz1[i] -= sumvz;
  }

  // cout << endl << "vx1[0]: " << vx1[0] << "   vy1[0]: " << vy1[0];
  // cout << endl << "vx1[1]: " << vx1[1] << "   vy1[1]: " << vy1[1];
}
