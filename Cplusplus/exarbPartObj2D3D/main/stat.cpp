/* 
   stat.cpp
   
   Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>
#define _USE_MATH_DEFINES 
#include <math.h> 
#include <fstream>
// #include <string>

using namespace std;

#include "data.h"
#include "stat.h"

Stat::Stat(Data *data) {
  d = data;
  countSets = 0;
  maxAvgDist = 0.0;
}

void Stat::updateInData() {
  dimension = d->dimension;
  dots = d->dots;
  x1 = d->x1;
  y1 = d->y1;
  z1 = d->z1;
  a1 = d->a1;
  xs = d->xs;
  ys = d->ys;
  zs = d->zs;
  scale = d->scale;
  maxHist = d->maxHist;
  delr = d->delr;
  hist = d->hist;
  gRadDist = d->gRadDist;
  gRadDist2 = d->gRadDist2;
  histAdd = d->histAdd;
}

/*
Makes the list a1[dots] with the nearest particles to every particle.
*/
void Stat::initListOnlyClosest() {

  double smallest;
  int particle;
  double dx, dy, dz;
  double compare;
  double xt, yt, zt;

  for (int i = 0; i < dots; i++) {
    smallest = 1000000.0;
	xt = x1[i];
	yt = y1[i];
	zt = z1[i];

    for (int j = 0; j < i; j++) {
      dx = xt - x1[j];
      dy = yt - y1[j];
      dz = zt - z1[j];
      compare = dx * dx + dy * dy + dz * dz;
      
      if (compare < smallest) {
        smallest = compare;
		particle = j;
      }
    }

	for (int j = i + 1; j < dots; j++) {
      dx = xt - x1[j];
      dy = yt - y1[j];
      dz = zt - z1[j];
      compare = dx * dx + dy * dy + dz * dz;
      
      if (compare < smallest) {
        smallest = compare;
		particle = j;
      }
    }

	a1[i] = particle;
  }
}

// The average of the distance to every points nearest neighbour.
// Standarddeviation
// sigma = sqrt(sum((ri-average)^2)/n) where average = sum(ri) / n
// or sigma = sqrt(sum(ri^2)/n-(sum(ri)/n)^2) = sqrt(sum(ri^2)/n-(average)^2)
void Stat::averageDistToNearest(int *a1) {
  
  int temp1;
  double dx, dy, dz;
  double r, r2;
  double smallest = a1[0];
  double average;
  double rDots = (double)dots;
  double sDev;
  double sumR = 0.0;
  double sumR2 = 0.0;

  for (int i = 0; i < dots; i++) {
    temp1 = a1[i];
    dx = x1[i] - x1[temp1];
    dy = y1[i] - y1[temp1];
    dz = z1[i] - z1[temp1];

	dx += (dx < -0.5) - (dx > 0.5);
	dy += (dy < -0.5) - (dy > 0.5);
	dz += (dz < -0.5) - (dz > 0.5);

	r2 = dx * dx + dy * dy + dz * dz;
    sumR2 += r2;
	r = sqrt(r2);
    sumR += r;

	if (r < smallest) {
	  smallest = r;
	}
  }
  
  average = sumR / rDots;
  sDev = sqrt(sumR2 / rDots - (average * average));

  if (average > maxAvgDist) {
    maxAvgDist = average;
  }

  cout << endl << "Average distance to nearest particle: "
      << average << "  stand. deviation: " << sDev;
  cout << endl << "Smallest distance to any particle: " << smallest;
}

/*
Adds the distance for every particle to every other particle. Then takes
the mean of all distances.
*/
void Stat::averageDistForAll() {
  double dist = 0;
  double dots2 = (double) dots;
  double dx, dy, dz;

  for (int i = 0; i < dots; i++) {
    for (int j = i + 1; j < dots; j++) {
      dx = x1[i] - x1[j];
      dy = y1[i] - y1[j];
      dz = z1[i] - z1[j];
      dist += 2 * sqrt(dx * dx + dy * dy + dz * dz);
	}
  }
  
  // dots * dots - "the dots in the middle of the matrix"
  // cout << endl << "Average distance between particles for all particles: "
  //    << dist / (dots2 * dots2 - dots2);
}

// Root mean square of the deplacement from the original configuration.
// rms = squareRoot((sum(r^2) for all particles) / number of particles)
double Stat::deplacementRms() {
  double rms = 0.0;
  double dx, dy, dz;

  for (int i = 0; i < dots; i++) {
    dx = x1[i] - xs[i];
    dy = y1[i] - ys[i];
    dz = z1[i] - zs[i];
    rms += dx * dx + dy * dy + dz * dz;
  }

  return sqrt(rms / ((double)dots));
}

/*
Configurations are read in turn and Rij for all pairs of atoms are calculated.
The Rij:s are sorted into a histogram where each bin has a width dr (delr)
and extends from r to r+dr.
int bin
maxbin: maxsize of hist array
delr: dr (from r to r+dr)
i: 1, N-1
j: i+1, N
  Rij = sqrt(dx*dx+dy*dy+dz*dz)
  bin = int(Rij/delr) + 1
  // Limit calculation to less than or equal to half the box-length.
  if (bin <= maxbin) {
    hist(bin) = hist(bin) + 2
  }
The ij and ji separations are sorted simultaneously.
When all the configurations have been processed, the histogram �hist� must be
normalized to calculate g(r). Suppose there are T configurations and a
particular bin b of the histogram, corresponding to the interval (r, r+dr),
contains nhis(b) pairs. Then the average number of atoms whose distance from a
given atom in the fluid lies in this interval, is
n(b) = nhis(b) / (n * T)
The average number of atoms in the same interval in an ideal gas at the same density rho is
nid(b) = 4 * pi * rho / 3 * ((r + dr)^3 � r^3)
By definition, the radial distribution function is
g(r + 1 / 2 * dr) = n(b) / nid(b)
The code for normalizing HIST is
double NIDEAL
CONST = 4.0 * PI * RHO / 3.0
DO 10 BIN = 1, MAXBIN
  RLOWER = REAL ( BIN � 1 ) * DELR
  RUPPER = RLOWER + DELR
  NIDEAL = CONST * ( RUPPER ** 3 � RLOWER ** 3 )
  GR(BIN) = REAL(HIST(BIN)) / REAL(NSTEP) / REAL(N) / NIDEAL
CONTINUE
The appropriate distance for a particular element of our g(r) histogram is at the centre of the interval (r, r + dr). i.e. at RLOWER+DELR/2 in the above example.
s. 183-184, bok �Computer Simulation of Liquids�
*/

/*
Configurations are read in turn and Rij for all pairs of atoms are calculated.
The Rij:s are sorted into a histogram where each bin has a width dr (delr)
and extends from r to r+dr. The calculation is limited to less than or equal to
half the box-length.
int bin
maxHist: maxsize of hist array
delr: dr (from r to r+dr)

The ij and ji separations are sorted simultaneously.
When all the configurations have been processed, the histogram "hist" must be
normalized to calculate g(r). Suppose there are T configurations and a
particular bin b of the histogram, corresponding to the interval (r, r+dr),
contains nhis(b) pairs. Then the average number of atoms whose distance from a
given atom in the fluid lies in this interval, is
n(b) = nhis(b) / (n * T)
The average number of atoms in the same interval in an ideal gas at the same density rho is
nid(b) = 4 * pi * rho / 3 * ((r + dr)^3 � r^3)
By definition, the radial distribution function is
g(r + 1 / 2 * dr) = n(b) / nid(b)
*/

void Stat::radialDistribution() {
  if (dimension == 2) {
	radialDistribution2D();
  } else {
    radialDistribution3D();
  }
}

void Stat::radialDistribution2D() {
  double dx, dy;
  double r;
  int bin;
  int number = 0;

  double nIdeal;
  // density rho is ((double)dots) / (1.0 * 1.0 * 1.0)
  const double rho = (double)dots;
  // const double shell = 4.0 * M_PI * rho / 3.0;
  const double shell = M_PI * rho;
  double rLower, rUpper;

  for (int i = 0; i < maxHist; i++) {
    hist[i] = 0;
	gRadDist2[i] = gRadDist[i];
	gRadDist[i] = 0.0;
  }

  if ((d->protons) == 0) {
    for (int i = 0; i < (dots - 1); i++) {
      for (int j = i + 1; j < dots; j++) {
// dx = abs(x1[i] - x1[j]);
// dx -= (dx > 0.5);

        dx = x1[i] - x1[j];
        dy = y1[i] - y1[j];

	    dx += (dx < -0.5) - (dx > 0.5);
	    dy += (dy < -0.5) - (dy > 0.5);

        r = dx * dx + dy * dy;

	    if (r <= (0.5 * 0.5)) {
	      bin = int(sqrt(r) / delr);
      
	      if (bin < maxHist) {
            hist[bin] = hist[bin] + 2;
          }
	    }
	  }
    }
  } else {
    for (int i = 0; i < (dots - 1); i++) {
      dx = x1[i];
      dy = y1[i];

      r = dx * dx + dy * dy;

	  if (r <= (0.5 * 0.5)) {
	    bin = int(sqrt(r) / delr);
      
	    if (bin < maxHist) {
          hist[bin] = hist[bin] + 1;
        }
	  }
    }
  }

  for (int i = 0; i < maxHist; i++) {
    rLower = ((double)i) * delr;
	rUpper = rLower + delr;
	nIdeal = shell * (rUpper * rUpper - rLower * rLower);
	gRadDist[i] = hist[i] / ((double)dots * nIdeal);
  }
}

void Stat::radialDistribution3D() {
  double dx, dy, dz;
  double r;
  int bin;
  int number = 0;

  double nIdeal;
  // density rho is ((double)dots) / (1.0 * 1.0 * 1.0)
  const double rho = (double)dots;
  const double shell = 4.0 * M_PI * rho / 3.0;
  double rLower, rUpper;

  for (int i = 0; i < maxHist; i++) {
    hist[i] = 0;
	gRadDist2[i] = gRadDist[i];
	gRadDist[i] = 0.0;
  }

  if ((d->protons) == 0) {
    for (int i = 0; i < (dots - 1); i++) {
      for (int j = i + 1; j < dots; j++) {
// dx = abs(x1[i] - x1[j]);
// dx -= (dx > 0.5);

        dx = x1[i] - x1[j];
        dy = y1[i] - y1[j];
        dz = z1[i] - z1[j];

	    dx += (dx < -0.5) - (dx > 0.5);
	    dy += (dy < -0.5) - (dy > 0.5);
	    dz += (dz < -0.5) - (dz > 0.5);

        r = dx * dx + dy * dy + dz * dz;

	    if (r <= (0.5 * 0.5)) {
	      bin = int(sqrt(r) / delr);
      
	      if (bin < maxHist) {
            hist[bin] = hist[bin] + 2;
          }
	    }
	  }
    }
  } else {
    for (int i = 0; i < (dots - 1); i++) {
      dx = x1[i];
      dy = y1[i];
	  dz = z1[i];

      r = dx * dx + dy * dy + dz * dz;

	  if (r <= (0.5 * 0.5)) {
	    bin = int(sqrt(r) / delr);
      
	    if (bin < maxHist) {
          hist[bin] = hist[bin] + 1;
        }
	  }
    }
  }

  for (int i = 0; i < maxHist; i++) {
    rLower = ((double)i) * delr;
	rUpper = rLower + delr;
	nIdeal = shell * (rUpper * rUpper * rUpper - rLower * rLower * rLower);
	gRadDist[i] = hist[i] / ((double)dots * nIdeal);
  }
}

double Stat::leastSquare() {
  double sum = 0.0;
  double diff;

  for (int i = 0; i < maxHist; i++) {
    diff = gRadDist[i] - gRadDist2[i];
	sum += diff * diff;
  }

  // cout << endl << "Rms of differences for radial distribution: "
  //		  << sqrt(sum / ((double)maxHist));

  return sum;
}

void Stat::addHist() {
  for (int i = 0; i < maxHist; i++) {
	histAdd[i] += hist[i];
  }

  countSets++;
}

void Stat::normalizeHistSets() {
  if (dimension == 2) {
	normalizeHistSets2D();
  } else {
    normalizeHistSets3D();
  }
}

void Stat::normalizeHistSets2D() {
  double nIdeal;
  // density rho is ((double)dots) / (1.0 * 1.0)
  const double rho = (double)dots;
  const double shell = M_PI * rho;
  double rLower, rUpper;

  for (int i = 0; i < maxHist; i++) {
    rLower = ((double)i) * delr;
	rUpper = rLower + delr;
	nIdeal = shell * (rUpper * rUpper - rLower * rLower);
	gRadDist[i] = histAdd[i] / ((double)countSets * (double)dots * nIdeal);
  }
}

void Stat::normalizeHistSets3D() {
  double nIdeal;
  // density rho is ((double)dots) / (1.0 * 1.0 * 1.0)
  const double rho = (double)dots;
  const double shell = 4.0 * M_PI * rho / 3.0;
  double rLower, rUpper;

  for (int i = 0; i < maxHist; i++) {
    rLower = ((double)i) * delr;
	rUpper = rLower + delr;
	nIdeal = shell * (rUpper * rUpper * rUpper - rLower * rLower * rLower);
	gRadDist[i] = histAdd[i] / ((double)countSets * (double)dots * nIdeal);
  }
}

// Writes histogram to file.
// C++ String Literals: http://msdn.microsoft.com/en-us/library/69ze775t.aspx
// char *text = "name";
// char text[] = "name";
void Stat::writeFile(char *fileName) {
  double xAxis = delr / 2.0;
  ofstream outputFile;

  outputFile.open(fileName);
  
  // cout << setiosflags(ios :: left);
  // cout << setw(10);

  if (outputFile.good())
  {
    outputFile << (maxHist - 1) << endl;
	// Mathematica comments are delimited by "(*" and "*)".
    outputFile << "(* Size of bins: " << delr << " *)" << endl;

    for (int i = 0; i < maxHist; i++) {
	  outputFile << gRadDist[i] << endl;
	  cout << endl << i << "  " << xAxis << "  " << histAdd[i] << "  " << gRadDist[i];
	  xAxis += delr;
    }
    
    outputFile.close();
  } else {
    outputFile.clear();
  }    
}

double Stat::getMaxAvgDist() {
  return maxAvgDist;
}

// Writes variables to file.
void Stat::writeCoordinates(const char *fileName) {
  ofstream outputFile;

  outputFile.open(fileName);

  if (outputFile.good())
  {
    outputFile << dots << endl;

	if (dimension == 2) {
      for (int i = 0; i < dots; i++) {
	    outputFile << x1[i] << "  " << y1[i] << endl;
      }
	} else {
	  for (int i = 0; i < dots; i++) {
	    outputFile << x1[i] << "  " << y1[i] << "  " << z1[i] << endl;
      }
	}

	outputFile.close();
  } else {
    outputFile.clear();
  }    
}
