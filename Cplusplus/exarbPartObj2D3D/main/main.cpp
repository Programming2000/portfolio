/* 
   Program
   
   Thomas Nilson
*/
 
#define WIN32_LEAN_AND_MEAN 

#include "stdafx.h"

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdlib>
// #include <windows.h>
#include <conio.h>
#include <ctime>
#define _USE_MATH_DEFINES
#include <math.h>

#define ALLEGRO_USE_CONSOLE 
#include <allegro.h>
#include <winalleg.h>

#include "data.h"
#include "stat.h"
#include "setup.h"
#include "timer.h"
#include "movement2D.h"
#include "movement3D.h"

using namespace std;

void gotoXY(int x, int y);
void getCursorXY(int &x, int&y);
void show(int dimension, int iteration, Timer &clock2);
void show1(int dimension, int iteration, Timer &clock2);
void show2(int dimension, int iteration, Timer &clock2);
// void noYes(bool choice);
void startMenu();
int enterLineInt(string text, int now, int low, int high);
double enterLineDouble(string text, double now);
string enterLineString(string text);
void choiceLine();
int menuInput();
void readVariables(const char *fileName);
void writeVariables(const char *fileName);
void showHelp();

/*
Variables that are used in more than one class or an array that reserves
memory is in the class data. Variables are then passed to the main program
and different classes so that they are present locally but points to the
same place in memory. This minimizes passing of variables to the
methods and collects data in a class. Performance is increased by having
a local pointer or copy of the data so that it doesn't have to be get through
a get-method in the class data. 
An object from the class data is created in main.cpp. The object is known by
the classes setup, stat, movement2D and movement3D. Constants from main are
passed to the class data. Then memory is reserved for arrays. Local pointers
in the main program gets to point at the corresponding variable in the class
data. Through the method updateInData in the various classes local variables
are set. If two pointers should be swapped they have to be swapped by a method
in the class data.
*/

// Scale: One unit [-0.5, 0.5].
// Metal atom: radius ca 100-150 pm => diameter 3.0e-10 m. Ten side by side => 3.0e-9.
// Wigner crystal. A uniform electron gas at zero temperature.
// Chrystalization at about Weigner-Seitz radius rs = 106 in 3D. Average interparticle spacing a = rs * ab.
// Bohr radius ab = 5.29e-11. a = 106 * 5.29e-11 = 5.61e-9.
// http://en.wikipedia.org/wiki/Electron_liquid
// http://en.wikipedia.org/wiki/Wigner_crystal
// http://en.wikipedia.org/wiki/Wigner%E2%80%93Seitz_radius
// http://en.wikipedia.org/wiki/Mean_inter-particle_distance
// scale = a / (mean interparticle distance from simulation) =
// 5.61e-9 / (mean interparticle distance from simulation)
// In 3D: scale = 5.61e-9 / pow(3.0 / (4.0 * M_PI * dots), 1.0 / 3.0)
// In 2D: scale = (31 * 5.29e-11) / sqrt(1.0 / (M_PI * dots)) =
// 1.6399e-9 / (1.0 / sqrt(M_PI * dots)) = 1.6399e-9 * sqrt(M_PI * dots)

// Every value marked "/**/" can be changed.

// Number of particles. Choose a number side^2 in 2D and side^3 in 3D so that
// they are placed evenly.
/**/int dots = 343;
// Dimension, 2D=2, 3D=3
/**/int dimension = 3;
// 2D, rs >= 31
double rsRadiusMeanDist2D = 1.0 / sqrt(M_PI * ((double)dots));
double wsRadius2D = sqrt(2.0) * 31.0;
double scale2D = wsRadius2D * 5.29e-11 / rsRadiusMeanDist2D;
// 3D, rs >= 106
double rsRadiusMeanDist3D = pow(3.0 / (4.0 * M_PI * ((double)dots)), 1.0 / 3.0);
double wsRadius3D = pow(2.0, 1.0 / 3.0) * 106.0;
double scale3D = wsRadius3D * 5.29e-11 / rsRadiusMeanDist3D;

// Number of protons in the center at (0, 0, 0).
/**/int protons = 0;
// Scale when there are protons present.
// R=Ro*A^(1/3), Ro=1.2e-15, A=atomic number
/**/double scaleWhenProtons = 8 * 5.29e-11; // 0.5 * 2.0e-10;
// Radius for cutoff in scale units [-0.5, 0.5].
/**/double cutOffProtons = 0.05;

// * dt = timestep. See method move in movement.cpp.
// See "sqrt(lengthR)/sqrt(r2)" for number of particles moving too long.
/**/double dt = 5.0e-16;
// Calculate timestep insread. Recalculation after every temperaturechange.
/**/bool calculateDt = true;
// * Temperature at start of phase 1 in kelvin.
/**/double startTemp = 25.0;
// Temperature after the cooling process in phase 2 is finished.
/**/double stopTemp = 12.5;

// Phase 1: Iterations in at temperature startTemp.
/**/int processings1 = 500;
// Interval in phase 1 for writing information.
// "Number of ((sqrt(lengthR) / sqrt(r2)) > 0.1):" shows number of particles
// which moves to long that iteration.
// (Use "every = 20" in combination with "movement.writeEnergy(false)"
// to check energyfluctuations for a certain value of the constants
// dt and startingVelocities.)
/**/int every1 = 500;
// Phase 2: Number of iterations in the cooling process from startTemp to
// stopTemp.
/**/int processings2 = 3500;
// Interval in phase 2 for writing information.
/**/int every2 = 3500;
// Phase 3: Number of iterations at temperature stopTemp.
/**/int processings3 = 3000;
// Interval in phase 3 for writing information.
/**/int every3 = 3000;

// Maximum frames per second for the graphics.
// Since the graphics are updated after an iteration and what takes time
// is the calculations for the force which have timecomplexity O(dots^2/2)
// this is a max value.
/**/double framesPerSecond = 60.0;
// Show xy (front), xz (top) and yz (side)
/**/bool threeDimView = false;

// If the positions of the particles will be deplaced by a random value
// +/- (original spacing between particles * 3 / 8) => minimum spacing = 1/4
// of original spacing
/**/bool randomDeplacement = false;

// Pauses and waits for a keypress during the writing of information in any
// of the processings.
// =0: Doesn't pause.
// =1: If any particle moves more than 5% of the distance to it's closest
//     neighbour.
// =2: If any particle moves more than 5% of rsRadiusMeanDist for 2D or 3D
//     which is calculated above.
/**/int pauseIfAnyMovesToFar = 0;
// dr (from r to r + dr) for the histogram. See radialDistribution
// in stat.cpp.
// Size of the bins for the histogram.
/**/double delr = 0.01;
// Filename for radial distribution function data
// Change "." to "," in the file and copy data to Excel.
// x-axis is in steps of "delr" above.
/**/char *file = "g(r).txt";

// Writes coordinates of all the particles
/**/bool few = false;

// F = k * q1 * q2 / r^2. See methods force and potentialEnergy
// in movement.cpp. k is in the unit [N * m^2 / C^2].
double ke = 8.9875517873681764e9 * 1.602176565e-19 * 1.602176565e-19;
// m = mass of particle. See methods move and kineticEnergy in
// movement.cpp.
double mass = 9.10938291e-31;
// This value is only used to initialize the velocities.
// Se temperature for setting of mobility.
// Velocities in [m / s] and then divided by scale.
// Random starting velocities for components vx, vy and vz:
// up to +/- startingVelocities. See method randomVelocities in
// setup.cpp.
double startingVelocities = 1000.0;
// Time in ms that has to pass before the graphics is updated.
// timeToFrame = 1000.0 / frames per second
double timeToFrame = 1000.0 / framesPerSecond;

double *x9;
double *y9;
double *z9;

BITMAP* frameBuffer;

const string noYes[2] = {"No", "Yes"};

int _tmain(int argc, _TCHAR* argv[]) {
// int main(int argc, char *argv[]) {

  double scale;
  double rsRadiusMeanDist;
 
  // Position in space.
  double *x1, *y1, *z1;
  // List of neighbours to particle.
  int *a1;
  
  Timer clock1;
  Timer clock2;

  double initialEnergy;
  double temp2 = startTemp;
  double deltaTemp = (startTemp - stopTemp) / ((double)processings2);

  int colorDepth;

  int countHistograms = 0;

  int start, stop;

  int status;

  // Randomize
  srand((unsigned int) time(0));

  // A new object data of the class data.
  Data *data = new Data();
  // The object data is known by the objects below.
  Setup setup(data);
  Stat stat(data);
  Movement2D movement2D(data);
  Movement3D movement3D(data);

  readVariables("config.txt");
  startMenu();
  status = menuInput();
  
  if (status == 2) {
    delete data;
    return 0;
  }

  temp2 = startTemp;
  deltaTemp = (startTemp - stopTemp) / ((double)processings2);

  rsRadiusMeanDist2D = 1.0 / sqrt(M_PI * ((double)dots));
  scale2D = wsRadius2D * 5.29e-11 / rsRadiusMeanDist2D;

  rsRadiusMeanDist3D = pow(3.0 / (4.0 * M_PI * ((double)dots)), 1.0 / 3.0);
  scale3D = wsRadius3D * 5.29e-11 / rsRadiusMeanDist3D;

  timeToFrame = 1000.0 / framesPerSecond;

  if (dimension == 2) {
	scale = scale2D;
	rsRadiusMeanDist = rsRadiusMeanDist2D;
  } else {
    scale = scale3D;
	rsRadiusMeanDist = rsRadiusMeanDist3D;
  }

  if (protons > 0) {
    scale = scaleWhenProtons;
  }

// Diameter of one watermolecule: 2.9e-9 m.

  // Constants are passed to the class data.
  // F=mass*a <=> a=F/mass [m/s^2]. a*dt^2/scale=F*dt^2/(mass*scale) gives v in
  // the [unit m/(scale*s)]. See start of move() in class movement2D and
  // movement3D.
  data->setData(dimension, dots, protons, delr, ke / (scale * scale), mass, dt, scale,
	  pauseIfAnyMovesToFar, rsRadiusMeanDist, calculateDt, cutOffProtons);
  // Memory is reserved.
  data->reserveMemory();
  // Local pointers in the main program gets to point at the corresponding
  // variable in the class data.

  setup.updateInData();
  stat.updateInData();
  movement2D.updateInData();
  movement3D.updateInData();

  // Local copies of pointer to memory in the class data.
  x1 = data->x1;
  y1 = data->y1;
  z1 = data->z1;
  a1 = data->a1;

  x9 = x1;
  y9 = y1;
  z9 = z1;

/*
setup.latticeBcc1a();
stat.radialDistribution();
stat.addHist();
stat.normalizeHistSets();
stat.writeFile(file);
delete data;
system("pause");
return 0;
*/

  // A random setup with a margin to the boundary.
  // setup.randomEvenDistr(0.1);

  // Places particles in a cubic formation in space.
  //setup.latticeStart();
  if (dimension == 2) {
	if (randomDeplacement) {
	  setup.latticeQuadraticWithDeplacement();
	} else {
	  setup.latticeQuadratic();
	}
  } else {
	if (randomDeplacement) {
	  setup.latticeCubicWithDeplacement();
	} else {
	  setup.latticeCubic();
	}
  }

  allegro_init();
  colorDepth = desktop_color_depth();
  set_color_depth(colorDepth);
  set_gfx_mode(GFX_AUTODETECT_WINDOWED, 480, 480, 0, 0);
  set_display_switch_mode(SWITCH_BACKGROUND);
  //frameBuffer = create_bitmap(480, 480);
  frameBuffer = create_bitmap_ex(colorDepth, 480, 480);

  show(dimension + 10, 0, clock2);

// system("pause>nul");

/*
for (int i = 0; i < dots; i++) {
  x1[i] = 0.0;
  y1[i] = 0.0;
  z1[i] = 0.0;
}

x1[0] = -0.25;
x1[1] = 0.0;
x1[2] = 0.25;
x1[3] = 0.50;
*/

  // A first calculation of force.
  if (dimension == 2) {
	movement2D.setupForce();
  } else {
    movement3D.setupForce();
  }

  // Random velocities. vx = [-startingVelocities, startingVelocities]
  // and the same for vy and vz.
  setup.randomVelocities(startingVelocities / scale);

  if (dimension == 2) {
	movement2D.temperature(temp2);
	movement2D.removeDrift();
  } else {
    movement3D.temperature(temp2);
	movement3D.removeDrift();
  }

  // Starts timer for frames
  clock2.startClock();

  cout << endl;
  cout << endl << "Startingconditions:";
  cout << endl << "Dimensions: " << dimension;
  cout << endl << "Number of particles: " << dots;

  if (dimension == 2) {
    cout << endl << "Wigner-Seitz radius in 2D: " << wsRadius2D;
  } else {
    cout << endl << "Wigner-Seitz radius in 3D: " << wsRadius3D;
  }

  cout << endl << "Scale: " << scale;
  cout << endl << "rs radius mean interparticle distance: " << rsRadiusMeanDist;
  cout << endl << "dr bins: " << delr;

  // Finds closest neighbours to all particles.
  stat.initListOnlyClosest();

/*
  // A faster way than "closest.initList(a1)" if particles >= 1000.
  cells.placeInCells();
  cells.initCellListCube();
  cells.initList2(maxDotsPerCell, a1);
*/

  // Adds the distance for every particle to every other particle. Then takes
  // the mean of all distances.
  stat.averageDistForAll();
  // The average of the distance to every points nearest neighbour.
  stat.averageDistToNearest(a1);
  
  if (dimension == 2) {
    // Sum of vx, sum of vy and sum of vz for all particles.
	movement2D.drift();
	// Calculates and writes kinetic and potential energy.
    movement2D.writeEnergy(false);
	initialEnergy = movement2D.kineticEnergy();
  } else {
    // Sum of vx, sum of vy and sum of vz for all particles.
    movement3D.drift();
	// Calculates and writes kinetic and potential energy.
    movement3D.writeEnergy(false);
	initialEnergy = movement3D.kineticEnergy();
  }
  
  cout << endl;

// system("pause>nul");

  // Starts timing
  clock1.startClock();

  start = 0;
  stop = processings1 + 1;

  for (int m = start; m < stop; m++) {
    // Writes information.
    if ((m % every1) == 0) {
	  cout << endl;
	  cout << endl << "After iteration: " << m;   
	  stat.initListOnlyClosest();
	}
    
	// An iteration in the moving algorithm with velocity Verlet.
	// A check is made to prevent the system to drift.
	// If abs(sumvx / mvx) > 1.0 or for y or z then the velocities are
	// changed so that sumvx, sumvy and sumvz are 0.
    if (dimension == 2) {
	  movement2D.move(m, every1);
    } else {
      movement3D.move(m, every1);
    }

    show(dimension, m, clock2);

	// Writes information.
    if ((m % every1) == 0) {
	  // Sum of vx, sum of vy and sum of vz for all particles.
      if (dimension == 2) {
	    movement2D.drift();
      } else {
        movement3D.drift();
      }
      // Calculates and writes kinetic and potential energy.
	  // Use argument "true" in combination with the constant every = 20 for
	  // a call to the method every 20 steps.
	  // The total energy shouldn't fluctuate more than 1 part in 5000 in
	  // 20 steps.
      if (dimension == 2) {
	    movement2D.writeEnergy(false);
      } else {
        movement3D.writeEnergy(false);
      }

      // Rms of deplacement from the original configuration.
      cout << endl << "Displacement rms, from initial position: "
		  << stat.deplacementRms();
      // Finds closest neighbours to all particles.
      stat.initListOnlyClosest();
      // Adds the distance for every particle to every other particle. Then takes
      // the mean of all distances.
//      stat.averageDistForAll();
      // The average of the distance to every points nearest neighbour.
      stat.averageDistToNearest(a1);
	  // Writes minimal average distance to nearest during simulation.
	  cout << endl << "Maximum average distance to nearest during sim.: "
		  << stat.getMaxAvgDist();
	  stat.radialDistribution();
	  cout << endl << "Sum of squares of differences for radial distribution: "
		  << stat.leastSquare();
	  cout << endl << "Timestep: " << data->dt << " s";

	  if (!few) {
//	    cout << endl << "xyz: " << x1[0] << "  " << y1[0] << "  " << z1[0];
//	    cout << endl << "xyz: " << x1[1] << "  " << y1[1] << "  " << z1[1];
//        cout << endl;
	  } else {
        for (int i = 0; i < dots; i++) {
//		  cout << endl << "xyz: " << x1[i] << "  " << y1[i] << "  " << z1[i];
		}

		cout << endl;
	  }
    }
  }

  start = processings1 + 1;
  stop = processings1 + processings2 + 1;

  for (int m = start; m < stop; m++) {
    // Cooling
    temp2 = temp2 - deltaTemp;

	if (abs(deltaTemp) > 1e-10) {
      if (dimension == 2) {
	    movement2D.temperature(temp2);
      } else {
        movement3D.temperature(temp2);
      }
	}

	// Writes information.
    if ((m % every2) == 0) {
	  cout << endl;
	  cout << endl << "After iteration: " << m;
	  stat.initListOnlyClosest();
	}
    
	// An iteration in the moving algorithm with velocity Verlet.
	// A check is made to prevent the system to drift.
	// If abs(sumvx / mvx) > 1.0 or for y or z then the velocities are
	// changed so that sumvx, sumvy and sumvz are 0.
    if (dimension == 2) {
	  movement2D.move(m, every2);
    } else {
      movement3D.move(m, every2);
    }
    
    show(dimension, m, clock2);

	// Writes information.
    if ((m % every2) == 0) {
	  // Sum of vx, sum of vy and sum of vz for all particles.
      if (dimension == 2) {
	    movement2D.drift();
      } else {
        movement3D.drift();
      }
      // Calculates and writes kinetic and potential energy.
	  // Use argument "true" in combination with the constant every = 20 for
	  // a call to the method every 20 steps.
	  // The total energy shouldn't fluctuate more than 1 part in 5000 in
	  // 20 steps.
       if (dimension == 2) {
	    movement2D.writeEnergy(false);
      } else {
        movement3D.writeEnergy(false);
      }

      // Rms of deplacement from the original configuration.
      cout << endl << "Displacement rms, from initial position: "
		  << stat.deplacementRms();
      // Finds closest neighbours to all particles.
      stat.initListOnlyClosest();
      // Adds the distance for every particle to every other particle. Then takes
      // the mean of all distances.
//      stat.averageDistForAll();
      // The average of the distance to every points nearest neighbour.
      stat.averageDistToNearest(a1);
	  // Writes minimal average distance to nearest during simulation.
	  cout << endl << "Maximum average distance to nearest during sim.: "
		  << stat.getMaxAvgDist();
	  stat.radialDistribution();
	  cout << endl << "Sum of squares of differences for radial distribution: "
		  << stat.leastSquare();
	  cout << endl << "Timestep: " << data->dt << " s";

	  if (!few) {
//	    cout << endl << "xyz: " << x1[0] << "  " << y1[0] << "  " << z1[0];
//	    cout << endl << "xyz: " << x1[1] << "  " << y1[1] << "  " << z1[1];
//        cout << endl;
	  } else {
        for (int i = 0; i < dots; i++) {
//		  cout << endl << "xyz: " << x1[i] << "  " << y1[i] << "  " << z1[i];
		}

		cout << endl;
	  }
    }
  }

  // Set velocities to the temperature for phase 3.
  if (dimension == 2) {
    movement2D.temperature(stopTemp);
  } else {
    movement3D.temperature(stopTemp);
  }

  start = processings1 + processings2 + 1;
  stop = processings1 + processings2 + processings3 + 1;

for (int m = start; m < stop; m++) {
	// Writes information.
    if ((m % every3) == 0) {
	  cout << endl;
	  cout << endl << "After iteration: " << m;
	  stat.initListOnlyClosest();
	}
    
	// An iteration in the moving algorithm with velocity Verlet.
	// A check is made to prevent the system to drift.
	// If abs(sumvx / mvx) > 1.0 or for y or z then the velocities are
	// changed so that sumvx, sumvy and sumvz are 0.
    if (dimension == 2) {
	  movement2D.move(m, every3);
    } else {
      movement3D.move(m, every3);
    }

    show(dimension, m, clock2);

	// Writes information.
    if ((m % every3) == 0) {
	  // Sum of vx, sum of vy and sum of vz for all particles.
      if (dimension == 2) {
	    movement2D.drift();
      } else {
        movement3D.drift();
      }
      // Calculates and writes kinetic and potential energy.
	  // Use argument "true" in combination with the constant every = 20 for
	  // a call to the method every 20 steps.
	  // The total energy shouldn't fluctuate more than 1 part in 5000 in
	  // 20 steps.
      if (dimension == 2) {
	    movement2D.writeEnergy(false);
      } else {
        movement3D.writeEnergy(false);
      }

      // Rms of deplacement from the original configuration.
      cout << endl << "Displacement rms, from initial position: "
		  << stat.deplacementRms();
      // Finds closest neighbours to all particles.
      stat.initListOnlyClosest();
      // Adds the distance for every particle to every other particle. Then takes
      // the mean of all distances.
//      stat.averageDistForAll();
      // The average of the distance to every points nearest neighbour.
      stat.averageDistToNearest(a1);
	  // Writes minimal average distance to nearest during simulation.
	  cout << endl << "Maximum average distance to nearest during sim.: "
		  << stat.getMaxAvgDist();
	  stat.radialDistribution();
	  cout << endl << "Sum of squares of differences for radial distribution: "
		  << stat.leastSquare();
	  cout << endl << "Timestep: " << data->dt << " s";

	  if (!few) {
//	    cout << endl << "xyz: " << x1[0] << "  " << y1[0] << "  " << z1[0];
//	    cout << endl << "xyz: " << x1[1] << "  " << y1[1] << "  " << z1[1];
//        cout << endl;
	  } else {
        for (int i = 0; i < dots; i++) {
//		  cout << endl << "xyz: " << x1[i] << "  " << y1[i] << "  " << z1[i];
		}

		cout << endl;
	  }

      stat.addHist();
      countHistograms++;
	  cout << endl << "Adding histogram number: " << countHistograms;
    }
  }

  show(dimension + 10, stop - 1, clock2);

  cout << endl;
  cout << endl << "Dimensions: " << dimension;
  cout << endl << "Number of particles: " << dots;
  cout << endl << "Timestep at stop: " << data->dt;
  cout << endl << "Time passed in simulation: " << data->sumDt << " s";
  cout << endl << "Start temperature: " << startTemp;
  cout << endl << "Stop temperature: " << stopTemp;

  if (dimension == 2) {
    cout << endl << "Wigner-Seitz radius in 2D: " << wsRadius2D;
  } else {
    cout << endl << "Wigner-Seitz radius in 3D: " << wsRadius3D;
  }

  cout << endl << "Scale: " << scale;
  cout << endl << "rs radius mean interparticle distance: " << rsRadiusMeanDist;
  cout << endl << "dr bins: " << delr;

  stat.radialDistribution();

// In loop 3 instead.  
// stat.addHist();
// countHistograms++;
  
  cout << endl << "Number of histograms added: " << countHistograms;

  // Stops timing.
  clock1.checkClock();
  // Writes passed time.
  cout << endl << "Time for computing: " << clock1.getDeltatimeInMs() / 1000.0 << " s";
  cout << endl;
  
  // The run was successful so save the variables from the menu.
  writeVariables("config.txt");

  // If only one sampling at the end.
  // stat.radialDistribution();

  stat.normalizeHistSets();
  stat.writeFile(file);

  stat.writeCoordinates("coordinates.txt");

  // Frees reserved memory for the arrays and deletes the data-object.
  delete data;

  destroy_bitmap(frameBuffer);

  cout << endl << endl;
  // "pause>nul" gives no text.
  system("pause>nul");
  // return EXIT_SUCCESS;
  return 0;
}

/*
Place cursor at (x, y) on the screen. (0, 0) at the top left of the screen.
@param x Positive values goes to the right
@param y Positive values goes down
*/
void gotoXY(int x, int y) {
  //Initialize the coordinates
  COORD coord = {x, y};
  
  //Set the position
  SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

/*
Gets the cursors position on the screen.
@param x X-axis
@param y Y-axis
*/
void getCursorXY(int &x, int &y) {
  CONSOLE_SCREEN_BUFFER_INFO csbi;

  if(GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi)) {
    x = csbi.dwCursorPosition.X;
    y = csbi.dwCursorPosition.Y;
  }
}

void show(int dimension, int iteration, Timer &clock2) {
  if ((threeDimView) && (dimension == 3 || dimension == 13)) {
    show2(dimension, iteration, clock2);
  } else {
	show1(dimension, iteration, clock2);
  }
}

/*
Plots all particles every time.
*/
void show1(int dimension, int iteration, Timer &clock2) {
  int xt, yt, zt;
  // char *text = 0;

  if (dimension <= 3) {
    clock2.checkClock();

    if (clock2.getDeltatimeInMs() < timeToFrame) {
      return;
    }
  } else {
    dimension -= 10;
  }

  clock2.startClock();

clear(frameBuffer);
// clear_to_color(frameBuffer, makecol(255, 255, 255));

acquire_screen();

  // _itoa_s(iteration, text, 65, 10);
  // textout_ex(frameBuffer, font, text, 400, 20, makecol(255, 255, 255), -1);

  textprintf_ex(frameBuffer, font, 400, 20, makecol(255, 255, 255), -1, "%d"
	  , iteration);

  // putpixel(frameBuffer, 240, 240, makecol(0, 255, 0));
  hline(frameBuffer, 239, 240, 241, makecol(0, 255, 0));
  vline(frameBuffer, 240, 239, 241, makecol(0, 255, 0));

  // setupShow();

  if (dimension == 2) {
    for (int i = 0; i < dots; i++) {
	  xt = (int)(x9[i] * 400.0) + 240;
	  yt = (int)(y9[i] * 400.0) + 240;

      putpixel(frameBuffer, xt, yt, makecol(255, 255, 255));
/*
	  if ((xt != x8[i]) || (yt != y8[i])) {
      // int q = 0;

        putpixel(frameBuffer, x8[i], y8[i], makecol(0, 0, 0));
        putpixel(frameBuffer, xt, yt, makecol(255, 255, 255));

	    x8[i] = xt;
	    y8[i] = yt;

//	    for (int p = 0; p < 1; p++) {
//	      q++;
//	    }
	  }
*/
    }
  } else {
    for (int i = 0; i < dots; i++) {
	  xt = (int)(x9[i] * 400.0) + 240;
	  yt = (int)(y9[i] * 400.0) + 240;
	  zt = (int)((z9[i] + 0.5) * 127.0) + 128; 
	  
	  if (zt < 0) {
	    zt = 0;
	  } else if (zt > 255) {
	    zt = 255;
	  }

      putpixel(frameBuffer, xt, yt, makecol(zt, zt, zt));
/*
	  if ((xt != x8[i]) || (yt != y8[i])) {
      // int q = 0;

        putpixel(frameBuffer, x8[i], y8[i], makecol(0, 0, 0));
        putpixel(frameBuffer, xt, yt, makecol(zt, zt, zt));

	    x8[i] = xt;
	    y8[i] = yt;

//	    for (int p = 0; p < 1; p++) {
//	      q++;
//	    }
	  }
*/
    }
  }

  // clear_to_color(screen, makecol( 0, 0, 0));
  blit(frameBuffer, screen, 0, 0, 0, 0, 480, 480);

  // The same as release_bitmap(screen).
  release_screen();
}

/*
xy, xz, yz.
*/
void show2(int dimension, int iteration, Timer &clock2) {
  int xt, yt, zt;
  int xi, yi, zi;
  // char *text = 0;

  if (dimension <= 3) {
    clock2.checkClock();

    if (clock2.getDeltatimeInMs() < timeToFrame) {
      return;
    }
  } else {
    dimension -= 10;
  }

  clock2.startClock();

clear(frameBuffer);
// clear_to_color(frameBuffer, makecol(255, 255, 255));

acquire_screen();

  // _itoa_s(iteration, text, 65, 10);
  // textout_ex(frameBuffer, font, text, 400, 20, makecol(255, 255, 255), -1);

  textprintf_ex(frameBuffer, font, 400, 20, makecol(255, 255, 255), -1, "%d"
	  , iteration);
  textprintf_ex(frameBuffer, font, 40, 20, makecol(255, 255, 255), -1
	  , "xy (front)   xz (top)   zy (side)");

  // putpixel(frameBuffer, 240, 240, makecol(0, 255, 0));
  hline(frameBuffer, 239, 240, 241, makecol(0, 255, 0));
  vline(frameBuffer, 240, 239, 241, makecol(0, 255, 0));

  // setupShow();

  if (dimension == 2) {
    for (int i = 0; i < dots; i++) {
	  xt = (int)(x9[i] * 400.0) + 240;
	  yt = (int)(y9[i] * 400.0) + 240;

      putpixel(frameBuffer, xt, yt, makecol(255, 255, 255));
    }
  } else {
    for (int i = 0; i < dots; i++) {
	  xt = (int)(x9[i] * 200.0);
	  yt = (int)(y9[i] * 200.0);
	  zt = (int)(z9[i] * 200.0);
	  
	  xi = (int)(x9[i] * 128.0) + 191;
	  yi = (int)(y9[i] * 128.0) + 191;
	  zi = (int)(z9[i] * 128.0) + 191;
	  
      putpixel(frameBuffer, xt + 140, yt + 140, makecol(zi, zi, zi));
	  putpixel(frameBuffer, xt + 340, zt + 140, makecol(yi, yi, yi));
      putpixel(frameBuffer, zt + 340, yt + 340, makecol(xi, xi, xi));
	  /*
      putpixel(frameBuffer, xt + 140, yt + 140, makecol(255, 255, 255));
	  putpixel(frameBuffer, xt + 340, zt + 140, makecol(255, 255, 255));
      putpixel(frameBuffer, zt + 340, yt + 340, makecol(255, 255, 255));
	  */
    }
  }

  // clear_to_color(screen, makecol( 0, 0, 0));
  blit(frameBuffer, screen, 0, 0, 0, 0, 480, 480);

  // The same as release_bitmap(screen).
  release_screen();
}

/*
void noYes(bool choice) {
  if (choice)  {
	cout << "Yes";
  } else {
    cout << "No";
  }
}
*/

void startMenu() {
  system("cls");
  gotoXY(1, 1);
  cout << "0. Help";
  gotoXY(1, 2);
  cout << "1. Run simulation";
  gotoXY(1, 3);
  cout << "2. Load variables from file";
  gotoXY(1, 4);
  cout << "3. Save variables to file";
  gotoXY(1, 5);
  cout << "a. Dimension: " << dimension;
  gotoXY(1, 6);
  cout << "b. Number of electrons: " << dots;
  gotoXY(1, 7);
  cout << "c. Wigner-Seitz radius in 2D: " << wsRadius2D;
  gotoXY(1, 8);
  cout << "d. Wigner-Seitz radius in 3D: " << wsRadius3D;
  gotoXY(1, 9);
  cout << "e: Number of protons: " << protons;
  gotoXY(1, 10);
  cout << "f: Side when protons present: " << scaleWhenProtons;
  gotoXY(1, 11);
  cout << "g: Cutoff when protons: " << cutOffProtons;
  gotoXY(1, 12);
  cout << "h: Timestep: " << dt;
  gotoXY(1, 13);
  cout << "i: Calculate all timesteps: " << noYes[calculateDt];
  gotoXY(1, 14);
  cout << "j: Iter. 1st phase: " << processings1;
  gotoXY(1, 15);
  cout << "k: Info. interval, 1st phase: " << every1;
  gotoXY(1, 16);
  cout << "l: Iter. 2nd phase when temp. changes: " << processings2;
  gotoXY(1, 17);
  cout << "m: Info. interval, 2nd phase: " << every2;
  gotoXY(1, 18);
  cout << "n: Iter. in 3rd phase: " << processings3;
  gotoXY(1, 19);
  cout << "o: Info. interval, 3rd phase: " << every3;

  gotoXY(42, 1);
  cout << "p: Start temp.: " << startTemp;
  gotoXY(42, 2);
  cout << "q: Stop temp.: " << stopTemp;
  gotoXY(42, 3);
  cout << "r: Fps for graphics: " << framesPerSecond;
  gotoXY(42, 4);
  cout << "s: Show front, top and side: " << noYes[threeDimView];
  gotoXY(42, 5);
  cout << "t: Dislocate to min. 1/4 spacing: " << noYes[randomDeplacement];
  gotoXY(42, 6);
  cout << "u: Histogram bins: " << delr;
  gotoXY(42, 7);
  cout << "4: Exit";
}

void choiceLine() {
  gotoXY(1,22);
  cout << "Choice: ";
}

/*
Questionline at the bottom of the screen.
text + "(" + now + ")" + "[" + low + ", " + high + "]: "
If low != -1 so show "(" + now + ")"
@param text Text to show
@param now  The current value
@param low  Lower bound
@param high Higher bound
@return The given answer
*/
int enterLineInt(string text, int now, int low, int high) {
  int answer;
  string answerString;
  
  gotoXY(1,22);
  cout << "       ";
  
  do {
    answerString = "";
    gotoXY(1,22);

	cout << text;

	if (low != -1) {
      cout << " [" << low << ", " << high << "]";
	}

	cout << " (integer, now: " << now << ") ";

    getline(cin, answerString);
    // For changing a string to an int.
    stringstream ss(answerString);
    // Tries to make the string an int.
    if ((ss >> answer).fail() || !(ss >> ws).eof()) {
      answer = now;
    }
  } while (answer < low || answer > high);
  
  gotoXY(1, 22);
  // Delete questionline.
  for (int i = 0; i < 50; i++) {
    cout << " ";
  } 
  
  return answer;
}

/*
Questionline at the bottom of the screen.
text + "(" + now + ")" + "[" + low + ", " + high + "]: "
If low != -1 so show "(" + now + ")"
@param text Text to show
@param now  The current value
@param low  Lower bound
@param high Higher bound
@return The given answer
*/
double enterLineDouble(string text, double now) {
  // Default value
  double newChoice = now;
  double answer;
  string answerString;
  
  gotoXY(1,22);
  cout << "       ";
  
  answerString = "";
  gotoXY(1,22);
  cout << text << " (floating point, now: " << now << ") ";

  getline(cin, answerString);
  // For changing a string to an double.
  stringstream ss(answerString);
  // Tries to make the string an double.
  if ((ss >> answer).fail() || !(ss >> ws).eof()) {
    answer = newChoice;
  }
  
  newChoice = answer;
  gotoXY(1, 22);
  // Delete questionline.
  for (int i = 0; i < 50; i++) {
    cout << " ";
  } 
  
  return newChoice;
}

string enterLineString(string text) {
  string answerString;
  
  gotoXY(1,22);
  cout << "       ";

  answerString = "";
  gotoXY(1,22);
  cout << text << ": ";
  getline(cin, answerString);
  
  gotoXY(1, 22);
  // Delete questionline.
  for (int i = 0; i < 50; i++) {
    cout << " ";
  } 
  
  return answerString;
}

int menuInput() {
  char ch = '\0';
  string file2 = "";
  int status = 0;

  fflush(stdin);
  gotoXY(1,22);
  cout << "Choice: ";

  do {
    if (ch != '\0') {
      startMenu();
	  gotoXY(1,22);
      cout << "Choice: ";
    }

    gotoXY(8, 22);
	ch = '\0';
    ch = _getch();
    
    switch (ch) {
      case '0':
		showHelp();
        break;
      case '1':
		status = 1;
        break;
      case '2':
	    file2 = enterLineString("2. Enter full filename");
		readVariables(file2.c_str());
        break;
      case '3':
	    file2 = enterLineString("3. Enter full filename");
		writeVariables(file2.c_str());
        break;
      case 'a':
		dimension = enterLineInt("a.", dimension, 2, 3);
        break;
      case 'b':
		dots = enterLineInt("b.", dots, 4, 10000);
        break;
      case 'c':
		wsRadius2D = enterLineDouble("c.", wsRadius2D); 
        break;
      case 'd':
		wsRadius3D = enterLineDouble("d.", wsRadius3D);
        break;
      case 'e':
		protons = enterLineInt("e.", protons, 0, 10000);
        break;
      case 'f':
		scaleWhenProtons = enterLineDouble("f.", scaleWhenProtons);
        break;
      case 'g':
		cutOffProtons = enterLineDouble("g. [0.0, 0.5]", cutOffProtons);
        break;
      case 'h':
		dt = enterLineDouble("h.", dt);
        break;
      case 'i':
		calculateDt = !calculateDt;
        break;
      case 'j':
		processings1 = enterLineInt("j.", processings1, 0, 100000000);
        break;
      case 'k':
		every1 = enterLineInt("k.", every1, 0, 100000000);
        break;
      case 'l':
		processings2 = enterLineInt("l.", processings2, 0, 100000000);
        break;
      case 'm':
		every2 = enterLineInt("m.", every2, 0, 100000000);
        break;
      case 'n':
		processings3 = enterLineInt("n.", processings3, 0, 100000000);
        break;
      case 'o':
		every3 = enterLineInt("o.", every3, 0, 100000000);
        break;
      case 'p':
		startTemp = enterLineDouble("p. [0,]", startTemp);
        break;
      case 'q':
		stopTemp = enterLineDouble("q. [0,]", stopTemp);
        break;
      case 'r':
		framesPerSecond = enterLineDouble("r. [0.01, 1000]", framesPerSecond);
        break;
      case 's':
		threeDimView = !threeDimView;
        break;
      case 't':
		randomDeplacement = !randomDeplacement;
        break;
      case 'u':
		delr = enterLineDouble("u. [0.001, 0.1]", delr);
        break;
      case '4':
		status = 2;
        break;

      Sleep(100);
    }
    // "0: Exit program"
  } while (status == 0);

  return status;
}

// Reads variables from file.
void readVariables(const char *fileName) {
  ifstream inputFile;

  inputFile.open(fileName);

  if (inputFile.good())
  {
    inputFile >> dimension;
    inputFile >> dots;
    inputFile >> wsRadius2D;
    inputFile >> wsRadius3D;
    inputFile >> protons;
    inputFile >> scaleWhenProtons;
    inputFile >> cutOffProtons;
    inputFile >> dt;
    inputFile >> calculateDt;
    inputFile >> processings1;
    inputFile >> every1;
    inputFile >> processings2;
    inputFile >> every2;
    inputFile >> processings3;
    inputFile >> every3;
    inputFile >> startTemp;
    inputFile >> stopTemp;
    inputFile >> framesPerSecond;
    inputFile >> threeDimView;
    inputFile >> randomDeplacement;
    inputFile >> delr;
    
    inputFile.close();
  } else {
    inputFile.clear();
  }    
}

// Writes variables to file.
void writeVariables(const char *fileName) {
  ofstream outputFile;

  outputFile.open(fileName);

  if (outputFile.good())
  {
    outputFile << dimension;
    outputFile << endl << dots;
    outputFile << endl << wsRadius2D;
    outputFile << endl << wsRadius3D;
    outputFile << endl << protons;
    outputFile << endl << scaleWhenProtons;
    outputFile << endl << cutOffProtons;
    outputFile << endl << dt;
    outputFile << endl << calculateDt;
    outputFile << endl << processings1;
    outputFile << endl << every1;
    outputFile << endl << processings2;
    outputFile << endl << every2;
    outputFile << endl << processings3;
    outputFile << endl << every3;
    outputFile << endl << startTemp;
    outputFile << endl << stopTemp;
    outputFile << endl << framesPerSecond;
    outputFile << endl << threeDimView;
    outputFile << endl << randomDeplacement;
    outputFile << endl << delr;

	outputFile.close();
  } else {
    outputFile.clear();
  }    
}

void showHelp() {
  system("cls");
  gotoXY(0, 1);
  cout << " Simulation takes place in a box with side [-0.5, 0.5] in scale"
	  << " units.";
  cout << endl;
  cout << " Dimension can be 2 or 3. Wigner-Seitz radius should be at least"
	  << " 31 in 2D and";
  cout << endl;
  cout << " 106 in 3D. Wigner-Seitz radius, dimension and number of"
	  << " electrons are used to";
  cout << endl;
  //       1                   80
  cout << " calculate the scale variable which is what 1.0 in the"
	  << " scale world means in";
  cout << endl;
  cout << " meter. Protons are placed in the middle.";
  cout << endl;
  cout << " If there is any protons the variable f. 'Side when protons"
	  << "present' is used";
  cout << endl;
  cout << " instead of W.-S. radius.";
  cout << endl;
  cout << " g. 'Cutoff when protons' in scale units gives the limit which"
	  << " below it there";
  cout << endl;
  cout << " is no force between electrons and protons.";
  cout << endl;
  cout << " h. Timestep should be in the order of 1.0e-16 and in status"
	  << " texts during";
  cout << endl;
  cout << " simulation the 'Number of moving > 5 %' should be close to"
	  << " zero.";
  cout << endl;
  cout << " It is recomended to use the 'Calculate all timesteps' because"
	  << " then the";
  cout << endl;
  cout << " timestep is recalculated after a change in temperature.";
  cout << endl;
  cout << " In j. to o. the iterations and status text interval is given"
	  << " for the three";
  cout << endl;
  cout << " phases. The temperature change takes place in the second"
	  << " phase.";
  cout << endl;
  cout << " p. 'Fps for graphics' is the frames per second for graphics."
	  << " Increase this if";
  cout << endl;
  cout << " you want the simulation to go slower.";
  cout << endl;
  cout << " q. 'Show front, top and side' gives graphics for a box which you"
	  << " look at from";
  cout << endl;
  cout << " the front, the top and from the side.";
  cout << endl;
  cout << " r. Gives a random dislocation from the standard quadratic or"
	  << " qubic lattice so";
  cout << endl;
  cout << " that there is at least 1/4 of the distance between compared to the"
	  << " distance";
  cout << endl;
  cout << " between when there is no dislocation.";
  cout << endl;
  cout << " s. Gives size of histogram bins in scale unit, 0.05 and 0.1 are"
	  << " good.";
  cout << endl;
  cout << " Histograms are sampled at the interval in phase 3 specified by"
	  << " 'Info.";
  cout << endl;
  cout << " interval, 3rd phase'.";
  cout << endl << " Press enter to return to menu.";
  system("pause>nul");
}
