/* 
   timer.h
   
   Thomas Nilson
*/

#ifndef TIMER_H
#define TIMER_H

class Timer {
  private:
    _LARGE_INTEGER freq;
    _LARGE_INTEGER time1, time2;
    double freq2, deltaTime;
  public:
    Timer();
    void startClock();
    void checkClock();
    double getDeltatimeInMs();
};

#endif
