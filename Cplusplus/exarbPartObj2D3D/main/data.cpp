/* 
   data.cpp
   
   Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>

#include "data.h"

using namespace std;

Data::Data() {
}

void Data::setData(int dimension, int dots, int protons, double delr, 
	double keScale, double mass, double dt, double scale, int pauseIfAnyMovesToFar,
	double rsRadiusMeanDist, bool calculateDt, double cutOffProtons) {

  this->dimension = dimension;
  this->dots = dots;
  this->protons = protons;
  this->cutOffProtons = cutOffProtons;
  this->delr = delr;
  this->keScale = keScale;
  this->mass = mass;
  this->dt = dt;
  this->calculateDt = calculateDt;
  this->scale = scale;

  maxHist = (int)(0.5 / delr);

  this->pauseIfAnyMovesToFar = pauseIfAnyMovesToFar;
  this->rsRadiusMeanDist = rsRadiusMeanDist;

  sumDt = 0.0;
}

void Data::reserveMemory() {
  x1 = new double [dots];
  y1 = new double [dots];
  z1 = new double [dots];

  vx1 = new double [dots];
  vy1 = new double [dots];
  vz1 = new double [dots];

  fx1 = new double [dots];
  fy1 = new double [dots];
  fz1 = new double [dots];

  fx2 = new double [dots];
  fy2 = new double [dots];
  fz2 = new double [dots];

  fx3 = new double [dots];
  fy3 = new double [dots];
  fz3 = new double [dots];
  fx4 = new double [dots];
  fy4 = new double [dots];
  fz4 = new double [dots];
  fx5 = new double [dots];
  fy5 = new double [dots];
  fz5 = new double [dots];

  xs = new double [dots];
  ys = new double [dots];
  zs = new double [dots];

  hist = new int [maxHist];
  gRadDist = new double [maxHist];
  gRadDist2 = new double [maxHist];
  histAdd = new int [maxHist];

  for (int i = 0; i < maxHist; i++) {
    gRadDist[i] = 0.0;
	histAdd[i] = 0;
  }

  a1 = new int [dots];
}

void Data::swapf1f2() {
  double *swap;

  swap = fx1;
  fx1 = fx2;
  fx2 = swap;

  swap = fy1;
  fy1 = fy2;
  fy2 = swap;

  swap = fz1;
  fz1 = fz2;
  fz2 = swap;
}

void Data::setDt(double newDt) {
  dt = newDt;
}

void Data::releaseMemory() {
  delete[] x1;
  delete[] y1;
  delete[] z1;

  delete[] vx1;
  delete[] vy1;
  delete[] vz1;

  delete[] fx1;
  delete[] fy1;
  delete[] fz1;

  delete[] fx2;
  delete[] fy2;
  delete[] fz2;

  delete[] fx3;
  delete[] fy3;
  delete[] fz3;
  delete[] fx4;
  delete[] fy4;
  delete[] fz4;
  delete[] fx5;
  delete[] fy5;
  delete[] fz5;

  delete[] xs;
  delete[] ys;
  delete[] zs;

  delete[] hist;
  delete[] gRadDist;
  delete[] gRadDist2;
  delete[] histAdd;

  delete[] a1;
}

Data::~Data() {
  releaseMemory();
}
