/* 
   movement3D.h
   
   Thomas Nilson
*/

#ifndef MOVEMENT3D_H
#define MOVEMENT3D_H

class Movement3D {
private:
  Data *d;
  int dots;
  int protons;
  double cutOffProtons;
  double *x1;
  double *y1;
  double *z1;
  double *vx1;
  double *vy1;
  double *vz1;
  double *fx1;
  double *fy1;
  double *fz1;
  double *fx2;
  double *fy2;
  double *fz2;

  double *fx3;
  double *fy3;
  double *fz3;
  double *fx4;
  double *fy4;
  double *fz4;
  double *fx5;
  double *fy5;
  double *fz5;

  double keScale;
  double mass;
  double dt;
  bool calculateDt;
  double scale;
  double reScale;

  double previous;

  int *a1;

  double minPot;
  double levelKinEnergy;
  double maxTemp;

  int pauseIfAnyMovesToFar;
  double rsRadiusMeanDist;

  int bytes;
public:
  Movement3D(Data *data);

  void updateInData();

  // To use a specific force change name in movement.h and movement.cpp so that
  // the force to use is called force. Call any other force another name.

  /*
  Calculation of force on each particle due to the Coulomb force.
  U = ke * Q1 * Q2 / r 
  F = ke * Q1 * Q2 / r^2
  Coulomb constant ke = 1 / (4 * PI * vacuum permitivity)
  */
  void force(double *fx, double *fy, double *fz, int start);

  /*
  Calculation of force on each particle due a Lennard-Jones potential.
  U = 3 * epsilon * ((sigma / r)^12 - (sigma / r)^6)
  F = 24 * epsilon / r * (2 * (sigma / r)^12 - (sigma / r)^6)
  */
  void force2(double *fx, double *fy, double *fz, int start);

  // double sqrtFast(double n);

  void setupForce();

  void forceCalc(double *fx, double *fy, double *fz);

  double kineticEnergy();

  double potentialEnergy();

  void writeEnergy(bool differenceToLastWriteout);

  void scaleEnergy(double newEnergy);

  void temperature(double temp);

  /*
  Sum of vx, sum of vy and sum of vz for all particles.
  */
  void drift();

  void removeDrift();

  void energy();

  void move(int processing, int every);

  void zeroVel();

  void lowerTemperature(double factor);

  double getMinPot();

  double getMaxTemp();
};

#endif
