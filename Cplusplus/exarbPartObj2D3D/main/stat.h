/* 
   stat.h
   
   Thomas Nilson
*/

#ifndef STAT_H
#define STAT_H

class Stat {
private:
  Data *d;
  int dimension;
  int dots;
  double *x1;
  double *y1;
  double *z1;
  int *a1;
  double *xs;
  double *ys;
  double *zs;
  double scale;
  int maxHist;
  double delr;
  int *hist;
  double *gRadDist;
  double *gRadDist2;
  int *histAdd;
  int countSets;
  double maxAvgDist;
public:
  Stat(Data *data);

  void updateInData();

  /*
  Makes the list a1[dots] with the nearest particles to every particle.
  */
  void initListOnlyClosest();

// The average of the distance to every points nearest neighbour.
void averageDistToNearest(int *a1);

/*
Adds the distance for every particle to every other particle. Then takes
the average of all distances.
*/
void averageDistForAll();

// Root mean square of the deplacement from the original configuration.
// rms = squareRoot((sum(r^2) for all particles) / number of particles)
double deplacementRms();

void radialDistribution();

void radialDistribution2D();

void radialDistribution3D();

double leastSquare();

void addHist();

void normalizeHistSets();

void normalizeHistSets2D();

void normalizeHistSets3D();

// Writes histogram to file.
// C++ String Literals: http://msdn.microsoft.com/en-us/library/69ze775t.aspx
// char *text = "name";
// char text[] = "name";
void writeFile(char *fileName);

double getMaxAvgDist();

void writeCoordinates(const char *fileName);
};

#endif
