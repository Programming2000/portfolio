/* 
   setup.h
   
   Thomas Nilson
*/

#ifndef SETUP_H
#define SETUP_H

#include <iostream>

class Setup {
private:
  Data *d;
  int dimension;
  int dots;
  int protons;
  double *x1;
  double *y1;
  double *z1;
  double *vx1;
  double *vy1;
  double *vz1;
  double *xs;
  double *ys;
  double *zs;
public:
  Setup(Data *data);

  // Sets local data to copy data and to point to data from class Data.
  void updateInData();

  void latticeStart();

  /*
  Makes a lattice with number of particles equal to dots.

  The margin to the sides, at -0.5 and 0.5, is the same as the spacing between
  particles.
  */
  void latticeQuadratic();

  void latticeQuadraticWithDeplacement();

  /*
  Makes a lattice with number of particles equal to dots.
  The integer part of the cube root of the number of particles gives the side
  length of the layers. The rest of the particles
  (dots mod (ceiling(cube root(dots))^2)) is added to the last layer.
  The margin to the sides, at -1.0 and 1.0, is the same as the spacing between
  particles.
  */
  void latticeCubic();

  void latticeCubicWithDeplacement();

  void latticeBcc1a();

  void latticeBcc1b();

  void latticeBcc2();

  double randomNumberDouble(double to);

  void randomEvenDistr(double margin);

  /*
  vx = [-maxComponent, maxComponent] and the same for vy and vz.
  */
  void randomVelocities(double maxComponent);

  
};

#endif
