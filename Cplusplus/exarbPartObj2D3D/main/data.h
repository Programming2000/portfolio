/* 
   data.h
   
   Thomas Nilson
*/

#ifndef DATA_H
#define DATA_H

/*
  The varibles in Data are public so they can be accessed with
  "variable name with datatype Data"."variable in Data". For
  example: cout << "Number of dots: " << dataObj.dots;".
*/
class Data {
public:
  int dimension;
  int dots;

  int protons;
  double cutOffProtons;
  
  double delr;
  double scale;

  int maxHist;

  double keScale;
  double mass;
  double dt;
  bool calculateDt;
  double sumDt;

  double *x1;
  double *y1;
  double *z1;
  double *vx1;
  double *vy1;
  double *vz1;
  double *fx1;
  double *fy1;
  double *fz1;
  double *fx2;
  double *fy2;
  double *fz2;
  int *a1;

  double *fx3;
  double *fy3;
  double *fz3;
  double *fx4;
  double *fy4;
  double *fz4;
  double *fx5;
  double *fy5;
  double *fz5;

  double *xs;
  double *ys;
  double *zs;

  int *hist;
  double *gRadDist;
  double *gRadDist2;
  int *histAdd;

  int pauseIfAnyMovesToFar;
  double rsRadiusMeanDist;

  Data();

  void setData(int dimension, int dots, int protons, double delr, 
	double keScale, double mass, double dt, double scale, int pauseIfAnyMovesToFar,
	double rsRadiusMeanDist, bool calculateDt, double cutOffProtons);

  void reserveMemory();

  void swapf1f2();

  void setDt(double newDt);

  void releaseMemory();

  ~Data();
};



#endif
