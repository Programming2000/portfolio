/* 
    movement3D.cpp  
   
    Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>
#include <cmath>

#include <omp.h>

#include "data.h"
#include "movement3D.h"

using namespace std;

/*
Starthastigheter till partiklarna.
Summan av vx, vy och vz ska vara noll.
Ber�kning av summan av vx, vy, vz.
Ber�kning av summan av v^2 => energin f�r systemet.
Efter ca 500 iterationer med f�rflyttning ska hastigheterna justeras.
*/

/*
Standard velocity algorithm of Verlet
r(t + dt) = r(t) + p(t) * dt / m + F(t) * (dt * dt) / (2 * m)
p(t + dt) = p(t) + (F(t) + F(t + dt)) * dt / 2
with r and v, p = m * v
r(t + dt) = r(t) + v(t) * dt + F(t) * (dt * dt) / (2 * m)
v(t + dt) = v(t) + (F(t) + F(t + dt)) * dt / (2 * m)
vova_04.pdf, eq. (2.12) s. 17
eller
r(t + dt) = r(t) + v(t) * dt + F(t) * (dt * dt) / (2 * m)
v(t + dt / 2) = v(t) + F(t) * dt / (2 * m)
v(t + dt)  = v(t + dt / 2) + F(t + dt)  / (2 * m)
s. 81, bok "Computer Simulation of Liquids"

Leapfrog integration
v(t + dt / 2) = v(t - dt / 2) + F(t) / m * dt
r(t + dt) = r(t) + v(t + dt / 2) * dt
http://www.mpibpc.mpg.de/home/grubmueller/teaching/2011_2012_WiSe_CBP1/PDFs/lecture_02.pdf, s. 11-12
(http://www.mpibpc.mpg.de/home/grubmueller/teaching/2011_2012_WiSe_CBP1/
2. Lecture)
*/

Movement3D::Movement3D(Data *data) {
  d = data;
  previous = 0.0;
  minPot = 1000000.0;
  levelKinEnergy = 1000000.0;
  maxTemp = 0.0;
}

void Movement3D::updateInData() {
  dots = d->dots;
  protons = d->protons;
  cutOffProtons = d->cutOffProtons;
  x1 = d->x1;
  y1 = d->y1;
  z1 = d->z1;
  vx1 = d->vx1;
  vy1 = d->vy1;
  vz1 = d->vz1;
  fx1 = d->fx1;
  fy1 = d->fy1;
  fz1 = d->fz1;
  fx2 = d->fx2;
  fy2 = d->fy2;
  fz2 = d->fz2;

  fx3 = d->fx3;
  fy3 = d->fy3;
  fz3 = d->fz3;
  fx4 = d->fx4;
  fy4 = d->fy4;
  fz4 = d->fz4;
  fx5 = d->fx5;
  fy5 = d->fy5;
  fz5 = d->fz5;

  a1 = d->a1;

  keScale = d->keScale;
  mass = d->mass;
  dt = d->dt;
  calculateDt = d->calculateDt;
  scale = d->scale;
  // Classical electron radius re = 2.82e-15 m.
  reScale =  2.0 * 2.82e-15 / scale;

  // The radius of the sphere containing one particle: rs.
  // For 1000 particles rs is about 0.062.
  // reScale = 2.0 * pow((3.0 / (4.0 * PI * (double)dots)), 1.0 / 3.0);

  pauseIfAnyMovesToFar = d->pauseIfAnyMovesToFar;
  rsRadiusMeanDist = (d->rsRadiusMeanDist);

  bytes = sizeof(double) * dots;
}

// To use a specific force change name in movement.h and movement.cpp so that
// the force to use is called force. Call any other force another name.

/*
Calculation of force on each particle due to the Coulomb force.
U = ke * Q1 * Q2 / r 
F = ke * Q1 * Q2 / r^2
Coulomb constant ke = 1 / (4 * PI * vacuum permitivity)
*/
void Movement3D::force(double *fx, double *fy, double *fz, int start) {
  double dx, dy, dz;
  double r2, ir3;
  double x1t, y1t, z1t;
  double fxi, fyi, fzi;

  for (int i = start; i < dots; i += 4) {
	x1t = x1[i];
	y1t = y1[i];
	z1t = z1[i];
    fxi = fx[i];
	fyi = fy[i];
	fzi = fz[i];

	for (int j = i + 1; j < dots; j++) {
      dx = x1t - x1[j];
      dy = y1t - y1[j];
      dz = z1t - z1[j];

	  // Periodic
	  dx += (dx < -0.5) - (dx > 0.5);
	  dy += (dy < -0.5) - (dy > 0.5);
	  dz += (dz < -0.5) - (dz > 0.5);

	  r2 = dx * dx + dy * dy + dz * dz;
      // r = sqrt(r2);

	  // If r > 2 * classical electron radius re = 2.82e-15 m. reScale = 2 * re / scale.
//      if (sqrt(r2) > reScale) {
		// Divides by r to normalize vectors dx, dy, dz.
	    ir3 = keScale / (r2 * sqrt(r2));
	    dx *= ir3;
	    dy *= ir3;
	    dz *= ir3;

	    fxi += dx;
	    fyi += dy;
	    fzi += dz;

	    fx[j] -= dx;
	    fy[j] -= dy;
	    fz[j] -= dz;
//      }
    }
    
	fx[i] = fxi;
	fy[i] = fyi;
	fz[i] = fzi;
  }
}

/*
Calculation of force on each particle due a Lennard-Jones potential.
U = 3 * epsilon * ((sigma / r)^12 - (sigma / r)^6)
F = 24 * epsilon / r * (2 * (sigma / r)^12 - (sigma / r)^6)
*/
void Movement3D::force2(double *fx, double *fy, double *fz, int start) {
  double dx, dy, dz;
  double r, r2, ir3;
  double x1t, y1t, z1t;
  double fxi, fyi, fzi;
  double term6;
  const double sigma = 1.0;
  const double epsilon = 1.0;
  const double sigma3 = sigma * sigma * sigma;

  for (int i = start; i < dots; i += 4) {
	x1t = x1[i];
	y1t = y1[i];
	z1t = z1[i];
    fxi = fx[i];
	fyi = fy[i];
	fzi = fz[i];

	for (int j = i + 1; j < dots; j++) {
      dx = x1t - x1[j];
      dy = y1t - y1[j];
      dz = z1t - z1[j];

	  dx += (dx < -0.5) - (dx > 0.5);
	  dy += (dy < -0.5) - (dy > 0.5);
	  dz += (dz < -0.5) - (dz > 0.5);

	  r2 = dx * dx + dy * dy + dz * dz;
      r = sqrt(r2);

	  // If r > classical electron radius re = 2.82e-15 m. reScale = re / scale.
	  if (r >= reScale) {
		term6 = sigma3 / (r2 * r2 * r2);
		// Divides by r^2 to normalize vectors dx, dy, dz.
	    ir3 = 24.0 * epsilon / r2 * term6 * (2.0 * term6 - 1.0);
	    dx *= ir3;
	    dy *= ir3;
	    dz *= ir3;

	    fxi += dx;
	    fyi += dy;
	    fzi += dz;

	    fx[j] -= dx;
	    fy[j] -= dy;
	    fz[j] -= dz;
      }
    }
    
	fx[i] = fxi;
	fy[i] = fyi;
	fz[i] = fzi;
  }
}

void Movement3D::setupForce() {
  forceCalc(fx1, fy1, fz1);
}

// Enable OpenMP 2.0 support: http://msdn.microsoft.com/en-us/library/fw509c3b%28v=vs.80%29.aspx
void Movement3D::forceCalc(double *fx, double *fy, double *fz) {
  int bytes = sizeof(double) * dots;

  memset(fx, 0, bytes);
  memset(fy, 0, bytes);
  memset(fz, 0, bytes);
  memset(fx3, 0, bytes);
  memset(fy3, 0, bytes);
  memset(fz3, 0, bytes);
  memset(fx4, 0, bytes);
  memset(fy4, 0, bytes);
  memset(fz4, 0, bytes);
  memset(fx5, 0, bytes);
  memset(fy5, 0, bytes);
  memset(fz5, 0, bytes);
  
  #pragma omp parallel sections num_threads(4)
  {
	#pragma omp section
	{ 
      force(fx, fy, fz, 0);
    }
    #pragma omp section
	{
      force(fx3, fy3, fz3, 1);
	}
    #pragma omp section
	{
      force(fx4, fy4, fz4, 2);
	}
    #pragma omp section
	{
      force(fx5, fy5, fz5, 3);
	}
  }
  
  for (int i = 0; i < dots; i++) {
    fx[i] += fx3[i] + fx4[i] + fx5[i];
	fy[i] += fy3[i] + fy4[i] + fy5[i];
	fz[i] += fz3[i] + fz4[i] + fz5[i];
  }

}

/*
Sum of vx, sum of vy and sum of vz for all particles.
*/
void Movement3D::drift() {
  double sumvx, sumvy, sumvz;

  sumvx = 0.0;
  sumvy = 0.0;
  sumvz = 0.0;

  for (int i = 0; i < dots; i++) {
	sumvx += vx1[i];
	sumvy += vy1[i];
	sumvz += vz1[i];
  }

  cout << endl << "Sum vx: " << sumvx << "   Sum vy: " << sumvy
      << "   Sum vz: " << sumvz;
}

void Movement3D::removeDrift() {
  double sumvx, sumvy, sumvz;
  double n = (double)dots;

  sumvx = 0;
  sumvy = 0;
  sumvz = 0;

  for (int i = 0; i < dots; i++) {
	sumvx += vx1[i];
	sumvy += vy1[i];
	sumvz += vz1[i];
  }

  sumvx = sumvx / n;
  sumvy = sumvy / n;
  sumvz = sumvz / n;

  for (int i = 0; i < dots; i++) {
    vx1[i] -= sumvx;
    vy1[i] -= sumvy;
    vz1[i] -= sumvz;
  }
}

double Movement3D::kineticEnergy() {
  double sumK = 0.0;

  for (int i = 0; i < dots; i++) {
	sumK += vx1[i] * vx1[i] + vy1[i] * vy1[i] + vz1[i] * vz1[i];
  }

  sumK = sumK * scale * scale * mass / 2.0;

  return sumK;
}

// Potential energy according to periodic system.
double Movement3D::potentialEnergy() {
  double dx, dy, dz;
  double r;
  double sumP = 0.0;

  for (int i = 0; i < (dots - 1); i++) {
    for (int j = i + 1; j < dots; j++) {
      dx = x1[i] - x1[j];
      dy = y1[i] - y1[j];
      dz = z1[i] - z1[j];;

	  dx += (dx < -0.5) - (dx > 0.5);
	  dy += (dy < -0.5) - (dy > 0.5);
	  dz += (dz < -0.5) - (dz > 0.5);

	  r = sqrt(dx * dx + dy * dy + dz * dz);
	  // r = sqrtFast(dx * dx + dy * dy + dz * dz);
	  sumP += 1.0 / r;
    }
  }

  // coulombConst = 8.9875517873681764e9, about 15 digits in a "double"
  // sumP = sumP * 8.9875517873681764e9

  sumP = sumP * keScale * scale;
  if (sumP < minPot) {
    minPot = sumP;
  }

  return sumP;
}

/*
Use argument "true" in combination with the constant every = 20 for
a call to the method every 20 steps.
The total energy shouldn't fluctuate more than 1 part in 5000 in
20 steps.
*/
void Movement3D::writeEnergy(bool differenceToLastWriteout) {
  double sumK = kineticEnergy();
  double sumP = potentialEnergy();
  double total = sumK + sumP;
  double kb = 1.3806488e-23;
  double temp;

  temp = 2.0 * sumK / (3.0 * ((double)dots) * kb);
  cout << endl << "Kinetic energy for system: " << sumK << " J";
  cout << endl << "Potential energy for system: " << sumP << " J";
  cout << endl << "Total energy for system: " << total << " J";
  cout << endl << "Gamma (avg. pot. E. / avg. kin E.): " << sumP / sumK;
  // Write minimal potential energy during simulation.
  cout << endl << "Minimum potential energy during sim.: " << minPot << " J";
  cout << endl << "Temperature: " << temp << " K";
/*  
  if (differenceToLastWriteout) {
	double changeInParts = abs((total - previous) / previous);
    
	cout << endl << "Change in total energy since last writeout: "
		<< changeInParts;
	cout << endl;
	
	if (changeInParts < 0.0002) {
	  cout << "Less ";
	} else {
	  cout << "More ";
	  cout << endl;
	}

	cout << "than 1 part in 5000.";
  }
*/
  if (temp > maxTemp) {
    maxTemp = temp;
  }

  previous = total;
}

void Movement3D::scaleEnergy(double newEnergy) {
  double nowEnergy;
  double change;

  nowEnergy = kineticEnergy();
  change = sqrt(newEnergy / nowEnergy);

  for (int i = 0; i < dots; i++) {
    vx1[i] *= change;
    vy1[i] *= change;
    vz1[i] *= change;
  }
}

void Movement3D::temperature(double temp) {
  double newEnergy;
  double kb = 1.3806488e-23;

  newEnergy = (temp * 3.0 * ((double)dots) * kb / 2.0);
  levelKinEnergy = newEnergy;
  scaleEnergy(newEnergy);

  if (calculateDt) {
    double v, t, newDt;

	// Average speed in scale units
	v = sqrt(newEnergy * 2.0 / mass) / scale;
	// Time to go the distance whan all particles are evenly spaced.
	// s=v*t <=> t=s/v
	t = rsRadiusMeanDist / v;
	newDt = t / 10.0;
	d->setDt(newDt);
	dt = newDt;
  }
}

void Movement3D::move(int processing, int every) {
  double m2 = 1.0 / (2.0 * mass);
  double dt2 = dt * dt;
  // F=mass*a <=> a=F/mass [m/s^2]. a*dt^2/scale=F*dt^2/(mass*scale) gives v in
  // the [unit m/(scale*s)].
  double dtm2 = m2 * dt / scale;
  double x1n, y1n, z1n;

double rx, ry, rz;
double mv, mdv;
double lengthR;
double maxR = 0.0;
int count1 = 0;
int count2 = 0;
int count3 = 0;
int temp1;
double dx, dy, dz;
double r2;


  double n = (double)dots;


  double maxMoveToClosest = 0.0;

mv = 0.0;
mdv = 0.0;

// r(t + dt) = r(t) + v(t) * dt + F(t) * (dt * dt) / (2 * m)
// v(t + dt) = v(t) + (F(t) + F(t + dt)) * dt / (2 * m)

  for (int i = 0; i < dots; i++) {

// For writeout of "max velo".
if ((processing % every) == 0) {
double moveClosest;

// Shorter form of rx = vx1[i] * dt + fx1[i] * dt * dt / (2.0 * m)
rx = (vx1[i] + fx1[i] * dtm2) * dt;
ry = (vy1[i] + fy1[i] * dtm2) * dt;
rz = (vz1[i] + fz1[i] * dtm2) * dt;

// rx = vx1[i] * dt + fx1[i] * dt2 * m2;
// ry = vy1[i] * dt + fy1[i] * dt2 * m2;
// rz = vz1[i] * dt + fz1[i] * dt2 * m2;

lengthR = rx * rx + ry * ry + rz * rz;
temp1 = a1[i];
dx = x1[i] - x1[temp1];
dy = y1[i] - y1[temp1];
dz = z1[i] - z1[temp1];
r2 = dx * dx + dy * dy + dz * dz;

if (lengthR > maxR) {
  maxR = lengthR;
}

  moveClosest = sqrt(lengthR) / sqrt(r2);

  if (moveClosest > maxMoveToClosest) {
    maxMoveToClosest = moveClosest;
  }

  if (moveClosest > 0.01) {
    count1++;

    if (moveClosest > 0.05) {
      count2++;

      if (moveClosest > 0.10) {
        count3++;
      }
    } 
  }
}

    // Remove drift
    removeDrift();

    // Shorter form of x1n = x1[i] + vx1[i] * dt + fx1[i] * dt * dt / (2.0 * m)
    x1n = x1[i] + (vx1[i] + fx1[i] * dtm2) * dt;
	x1n = x1n - (x1n > 0.5) + (x1n < -0.5);
	x1[i] = x1n;
    y1n = y1[i] + (vy1[i] + fy1[i] * dtm2) * dt;
	y1n = y1n - (y1n > 0.5) + (y1n < -0.5);
	y1[i] = y1n;
    z1n = z1[i] + (vz1[i] + fz1[i] * dtm2) * dt;
	z1n = z1n - (z1n > 0.5) + (z1n < -0.5);
	z1[i] = z1n;

/*
    x1n = x1[i] + vx1[i] * dt + fx1[i] * dt2 * m2;
	x1n = x1n - (x1n > 0.5) + (x1n < -0.5);
	x1[i] = x1n;
    y1n = y1[i] + vy1[i] * dt + fy1[i] * dt2 * m2;
	y1n = y1n - (y1n > 0.5) + (y1n < -0.5);
	y1[i] = y1n;
    z1n = z1[i] + vz1[i] * dt + fz1[i] * dt2 * m2;
	z1n = z1n - (z1n > 0.5) + (z1n < -0.5);
	z1[i] = 0;
*/
  }

  forceCalc(fx2, fy2, fz2);

/*
// For writeout of "max force (f)" and "max change (df)".
if ((processing % every) == 0) {
  for (int i = 0; i < dots; i++) {
	  double fvalue, fchange;

      fvalue = sqrt(vx1[i] * vx1[i] + vy1[i] * vy1[i] + vz1[i] * vz1[i]);
	  fchange = sqrt((fx1[i] + fx2[i]) * (fx1[i] + fx2[i]) + (fy1[i] + fy2[i]) * (fy1[i] + fy2[i])
		  + (fz1[i] + fz2[i]) * (fz1[i] + fz2[i])) * dtm2;

      double tchangex, tchangey, tchangez;

      if (fvalue > mv) {
        mv = fvalue;
      } 
    
      if (fchange > mdv) {
        mdv = fchange;
      }
	}

    cout << endl << "max velo (v): " << mf;
    cout << endl << "max change (dv): " << mdf;
  }
}
*/

  // Protons radius about 1.7e-15 m.
  // Classical electron radius re = 2.82e-15 m.
  // reScale = 2 * re.
  if (protons > 0) {
    double dx, dy, dz;
	double ke2 = keScale * (double)protons;
	double ir3;
	double r, r2;

	for (int i = 0; i < dots; i++) {
      dx = x1[i];
	  dy = y1[i];
	  dz = z1[i];
	  r2 = dx * dx + dy * dy + dz * dz;
      r = sqrt(r2);

	  if (r >= cutOffProtons) {
	    ir3 = ke2 / (r * r2);
	    fx2[i] = fx2[i] - dx * ir3;
	    fy2[i] = fy2[i] - dy * ir3;
		fz2[i] = fz2[i] - dz * ir3;
	  }
    }
  }


  for (int i = 0; i < dots; i++) {
    double vvalue, vchange;

    vx1[i] = vx1[i] + (fx1[i] + fx2[i]) * dtm2;
    //sumvx += vx1[i];
    vy1[i] = vy1[i] + (fy1[i] + fy2[i]) * dtm2;
	//sumvy += vy1[i];
	vz1[i] = vz1[i] + (fz1[i] + fz2[i]) * dtm2;
	//sumvz += vz1[i];

    // For writeout of "max velo (v)" and "max change (dv)".
    if ((processing % every) == 0) {
      vvalue = sqrt(vx1[i] * vx1[i] + vy1[i] * vy1[i] + vz1[i] * vz1[i]);
	  vchange = sqrt((fx1[i] + fx2[i]) * (fx1[i] + fx2[i]) + (fy1[i] + fy2[i]) * (fy1[i] + fy2[i])
		  + (fz1[i] + fz2[i]) * (fz1[i] + fz2[i])) * dtm2;

      if (vvalue > mv) {
        mv = vvalue;
      } 
    
      if (vchange > mdv) {
        mdv = vchange;
      }
    }
  }

  d->swapf1f2();
  fx1 = d->fx1;
  fy1 = d->fy1;
  fz1 = d->fz1;
  fx2 = d->fx2;
  fy2 = d->fy2;
  fz2 = d->fz2;

  if ((processing % every) == 0) {
	double maxMove = sqrt(maxR);
    cout << endl << "Max move (r): " << maxMove << "   "
		<< maxMove * scale << " m";
	cout << endl << "Max velo (v): " << mv << "   " << mv * scale
		<< " m/s";
    cout << endl << "Max change (dv): " << mdv << "   " << mdv * scale
		<< " m/s";
    cout << endl << "Number of moving percent of distance to closest.";
    cout << endl << " >10%: " << count3 << "     >5%: " << count2
		<< "     >1%: " << count1;
	cout << endl << "Max move relative to closest: "
		<< maxMoveToClosest * 100.0 << " %";
	cout << endl << "Max move relative to mean interpart. dist.: "
		<< maxMove / (2.0 * rsRadiusMeanDist) * 100.0 << " %";

    if (pauseIfAnyMovesToFar == 1) {
      if (count2 > 0) {
        system("pause>nul");
      }
    } else if (pauseIfAnyMovesToFar == 2) {
      if ((maxMove / (2.0 * rsRadiusMeanDist)) > 0.05) {
        system("pause>nul");
      }
    }
  }

  scaleEnergy(levelKinEnergy);
  d->sumDt += dt;
}

void Movement3D::zeroVel() {
  memset(vx1, 0, bytes);
  memset(vy1, 0, bytes);
  memset(vz1, 0, bytes);
}

void Movement3D::lowerTemperature(double factor) {
  for (int i = 0; i < dots; i++) {
    vx1[i] *= factor;
    vy1[i] *= factor;
    vz1[i] *= factor;
  }
}

double Movement3D::getMinPot() {
  return minPot;
}

double Movement3D::getMaxTemp() {
  return maxTemp;
}
