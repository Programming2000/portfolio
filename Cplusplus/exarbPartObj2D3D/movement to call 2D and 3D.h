/* 
   movement.h
   
   Thomas Nilson
*/

#ifndef MOVEMENT_H
#define MOVEMENT_H

class Movement {
private:
  Data *d;
  Movement2D *m2D;
  Movement3D *m3D;
  int dimension;
public:
  Movement(Data *data);

  void updateInData();

  void setupForce();

  double kineticEnergy();

  double potentialEnergy();

  void writeEnergy(bool differenceToLastWriteout);

  void temperature(double temp);

  void energy();

  void move(int processing, int every, int protons);

  void zeroVel();

  double getMinPot();

  double getMaxTemp();
};

#endif
