/* 
    movement2D.cpp  
   
    Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>
#include <cmath>

#include "data.h"
#include "movement.h"
#include "movement2D.h"
#include "movement3D.h"

using namespace std;

Movement::Movement(Data *data) {
  d = data;
  m2D = new Movement2D(data);
  m3D = new Movement3D(data);
}

void Movement::updateInData() {
  dimension = d->dimension;

  if (dimension == 2) {
    m2D.updateInData();
  } else {
    m3D.updateInData();
  }
}

void Movement::setupForce() {
  if (dimension == 2) {
    m2D.setupForce();
  } else {
    m3D.setupForce();
  }
}

double Movement::kineticEnergy() {
}

// Potential energy according to periodic system.
double Movement::potentialEnergy() {
}

/*
Use argument "true" in combination with the constant every = 20 for
a call to the method every 20 steps.
The total energy shouldn't fluctuate more than 1 part in 5000 in
20 steps.
*/
void Movement::writeEnergy(bool differenceToLastWriteout) {
 
}

void Movement::temperature(double temp) {
}

void Movement::move(int processing, int every, int protons) {
}

double Movement::getMinPot() {
  return minPot;
}

double Movement::getMaxTemp() {
  return maxTemp;
}
