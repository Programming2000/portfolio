/* 
   timer.cpp
   
   Thomas Nilson
*/

#include "stdafx.h"

#include <iostream>
#include <cstdlib>
#include <windows.h>

#include "timer.h"

using namespace std;

Timer::Timer() {
  QueryPerformanceFrequency(&freq);
  freq2 = (double)freq.QuadPart / 1000.0;
}

void Timer::startClock() {
  QueryPerformanceCounter(&time1);
}

void Timer::checkClock() {
  QueryPerformanceCounter(&time2);
  deltaTime = ((double)(time2.QuadPart - time1.QuadPart)) / freq2;
}

double Timer::getDeltatimeInMs() {
  return deltaTime;
}
