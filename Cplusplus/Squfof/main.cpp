/*
	Primes Squfof
	2013 12 06

	Antalet iterationer fr�n Primes ab.

	Factorizes integers up to 18,446,744,073,709,551,615
	which is a "unsigned long long" data type.

	Test:

	53 * 101 = 5353
	iterationer: 4

	521 * 1123 = 585083
	iterationer: 58

	50021 * 75011 = 3 752 125 231
	iterationer: 1262

	500 009 * 1 000 003 = 500 010 500 027
	iterationer: 42892

	1 000 003 * 1 000 037 = 1 000 040 000 111
	iterationer: 1

	prime number: 982 451 653

	5 000 011 * 1 000 003 = 5 000 026 000 033
	iterationer: 763934

	50 000 017 * 1 000 003 = 50 000 167 000 051
	iterationer: 18 428 931

	10 000 019 * 15 455 591 = 154 556 203 656 229
	15 digits
	k = 3
	Tid p� 2.1 MHz processor: 0.016 s
	
	87 999 997 * 100 050 001 = 8 804 399 787 849 997
	16 digits
	k = 7
	Tid p� 2.1 MHz processor: 0.0093 s
	  With testbit(1) for testing perfect square: 0.047 s
	
	499 999 993 * 749 555 539 = 374 777 764 253 111 227
	18 digits
	Tid p� 2.1 MHz processor: 0.012 s
	?: With testbit(1) for testing perfect square: 0.050 s
*/

#include "stdafx.h"
#include <iostream>
// #include <string>
// #include <cstdlib>
// #define _USE_MATH_DEFINES
// #include <math.h>
#include <windows.h>

#include "timer.h"

using namespace std;

int numberOfDigits(unsigned long long number);
unsigned long long gcd (unsigned long long b, unsigned long long a);

int _tmain(int argc, _TCHAR* argv[])
{
	/*
	int
	�2,147,483,648 to 2,147,483,647

	unsigned int
	0 to 4,294,967,295
  
	unsigned __int64
	unsigned long long
	0 to 18,446,744,073,709,551,615
	*/

	/*
	P[0]=floor(sqrt(N))
	Q[0]=1
	Q[1]=N-P[0]^2

	Repeat
	b=floor(floor(sqrt(N)+P[i-1])/Q[i])
	P[i]=bQ[i]-P[i-1]
	Q[i+1]=Q[i-1]+b(P[i-1]-P[i])
	until Q[i] is a perfect square
	
	Q[0]=sqrt(Q[i])
	// Corrected to -P[i] /T.N.
	b=floor((floor(sqrt[N])-P[i])/Q[0])
	P[0]=bQ[0]-P[i]
	Q[1]=(N-P[0]^2)/Q[0]
	
	Repeat
	b=floor(floor(sqrt(N)+P[i-1])/Q[i])
	P[i]=bQ[i]-P[i-1]
	Q[i+1]=Q[i-1]+b(P[i-1]-P[i])
	until P[i+1]=P[i]

	Then gcd(N,P[i]) is a non-trivial factor of N.

	colin.barker.pagesperso-orange.fr/lpa/big_squf.htm
	*/

	unsigned long long n;
	unsigned long long k;
	unsigned long long rootkn;
	unsigned long long p0, p1;
	unsigned long long q0, q1, q2;
	unsigned long long b1;
	unsigned long long number;
	unsigned long long b0;
	unsigned long long pOld;
	unsigned long long a, b;

	int digits;
	int counter1;
	int counter2;

	// Kan ha _int64 datatyp.
	Timer clock1;

	cout << endl << "Shanks square forms factorization";
	cout << endl;
	cout << endl << "Factorizes integers up to k * n < range for datatype unsigned long long";
	cout << endl << "in vs2010, about 1*10^19.";
	cout << endl;
	cout << endl << "Number n to factor: ";
	cin >> n;
	digits =  numberOfDigits(n);
	cout << endl << "Number of digits: " << digits;
	cout << endl;

	do {
		counter1 = 0;
		counter2 = 0;
		cout << endl << "Small integer k, try 1: ";
		cin >> k;

		clock1.startClock();

		// kn = k * n;
		rootkn = sqrtl(k * n);

		p0 = rootkn;
		q0 = 1;
		q1 = k * n - p0 * p0;
		q2 = q1;
	
		// cout << endl << "p0: " << p0 << "   q0: " << q0 << "   q1: " << q1;
		// cout << endl;
	
		do {
			b1 = (rootkn + p0) / q1;
			p1 = b1 * q1 - p0;
			q2 = q0 + b1 * (p0 - p1);

			// cout << endl << "b: " << b1 << "   p: " << p1 << "   q1: " << q1 << "   q2: " << q2;

			q0 = q1;
			q1 = q2;
			// pOld = p0;
			p0 = p1;

			// system("pause>nul");

			number = q2 % (unsigned long long)sqrtl(q2);
			counter1++;
		} while (number != 0 && counter1 < 50000);

		/*
		cout << endl;
		cout << endl << "Perfect square: " << q2;
		cout << endl;
		*/

		// Enligt wikipedia: b0 = (rootkn - p0) / q0;
		q0 = sqrtl(q2);
		b0 = (rootkn - p1) / q0;
		p0 = b0 * q0 + p1;
		q1 = (k * n - p0 * p0) / q0; // Correct

		// cout << endl << "p0: " << p0 << "   q0: " << q0 << "   q1: " << q1;
		// cout << endl;

		do {
			b1 = (rootkn + p0) / q1;
			p1 = b1 * q1 - p0;
			q2 = q0 + b1 * (p0 - p1);

			// cout << endl << "b: " << b1 << "   p: " << p1 << "   q1: " << q1 << "   q2: " << q2;

			q0 = q1;
			q1 = q2;

			pOld = p0;
			p0 = p1;

			// system("pause>nul");

			counter2++;
		} while (p1 != pOld && counter2 < 50000);

	// cout << endl;
	// cout << endl << "p: " << p1;
		
		a = 1;

		if (p1 == pOld) {
			a = gcd(n, p1);
		}
	} while (a == 1 || a == n);

	b = n / a;
	cout << endl;
	cout << endl << "gcd(" << n << ", " << p1 << ") = " << a;

	clock1.checkClock();

	cout << endl;
	cout << endl << "Time for computing: " << clock1.getDeltatimeInMs() / 1000.0 << " s";
	cout << endl << "Iterations in first loop: " << counter1;
	cout << endl << "Iterations in second loop: " << counter2;
	cout << endl;
	cout << endl << a << " * " << b << " = " << n;
	cout << endl;

	system("pause>nul");

	return 0;
}

int numberOfDigits(unsigned long long number) {
	int length = 0;

	while (number > 0) {
		number = number / 10;
		length ++;
	}

	return length;
}

unsigned long long gcd (unsigned long long b, unsigned long long a) {
	unsigned long long c;

	while (a != 0) {
		c = a;
		a = b % a;
		b = c;
	}
	
	return b;
}
