﻿using System;
using System.Text.RegularExpressions;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Regex pattern1 = new Regex(@"[bsq]");

            Console.WriteLine("Card Game");
            Console.WriteLine("You are being dealt a card. Guess if the next card will be bigger or smaller.");
            Console.WriteLine("Answer 'b' for bigger and 's' for smaller.");
            Console.WriteLine("Ace = 1, Knight = 11, Queen = 12, King = 13");
            Console.WriteLine("First answer how many cards you want to be dealt.");
            Console.WriteLine("You get 1 point for the first correct answer. For each correct answer the");
            Console.WriteLine("amount of points you get doubles. So you get 1, 2, 4, 8, ...");
            Console.WriteLine("I you answers correctly for the number of cards choosen you may score higher");
            Console.WriteLine("than the high score.");
            Console.WriteLine("Exit with 'q'.\n");

            Deck deck1 = new Deck();
            int flag1;
            int highscore = 0;

            do
            {
                flag1 = 0;

                deck1.Shuffle();
                string input;
                int points = 0;
                int add = 1;
                int total = 1;
                int nr = 1;

                Console.WriteLine("High score: {0}", highscore);
                int present1Number;
                Card present2;

                do
                {
                    Console.WriteLine("How many questions do you want to answer (1 to 52): ");
                    int.TryParse(Console.ReadLine(), out total);
                }
                while (total < 1 || total > 52);

                present2 = deck1.nextCard();
                present1Number = present2.Number;
                Console.WriteLine("Card nr {0}: {1}", nr, present2);

                do
                {
                    input = " ";

                    do
                    {
                        Console.WriteLine("bigger or smaller (b/s/q): ");
                        input = Console.ReadLine();
                    }
                    while (!pattern1.IsMatch(input));

                    if (input != "q")
                    {
                        present2 = deck1.nextCard();
                        Console.WriteLine("Card nr {0}: {1}", nr + 1, present2);

                        if (input == "b" && (present2.Number > present1Number))
                        {
                            points += add;
                            Console.WriteLine("Correct, bigger, you get {0} more points for a total of {1} points.", add, points);
                            add *= 2;
                        }
                        else if (input == "s" && (present2.Number < present1Number))
                        {
                            points += add;
                            Console.WriteLine("Correct, smaller, you get {0} more points for a total of {1} points.", add, points);
                            add *= 2;
                        }
                        else
                        {
                            flag1 = 1;
                            Console.WriteLine("Sorry, wrong");
                        }
                    }
                    else
                    {
                        flag1 = 2;
                    }

                    present1Number = present2.Number;
                    nr++;
                }
                while (flag1 == 0 && nr <= total);

                if (flag1 == 0)
                {
                    Console.WriteLine("You answered correctly on {0} questions out of {1}", nr - 1, total);

                    if (points > highscore)
                    {
                        Console.WriteLine("Congratulations a new high score: {0}, the old high score was {1}", points, highscore);
                        highscore = points;
                    }
                }

                Console.WriteLine();
            }
            while (flag1 != 2);
        }
    }
}
