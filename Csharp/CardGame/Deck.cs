﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    class Deck
    {
        Card[] cards = new Card[52];
        string[] value = new string[] {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        int[] number = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        int pos = -1; 

        public Deck()
        {
            int i = 0;

            for (int j = 0; j < 13; j++)
            {
                cards[i] = new Card(Suits.Hearts, value[j], number[j]);
                i++;
            }

            for (int j = 0; j < 13; j++)
            {
                cards[i] = new Card(Suits.Diamonds, value[j], number[j]);
                i++;
            }

            for (int j = 0; j < 13; j++)
            {
                cards[i] = new Card(Suits.Spades, value[j], number[j]);
                i++;
            }

            for (int j = 0; j < 13; j++)
            {
                cards[i] = new Card(Suits.Clubs, value[j], number[j]);
                i++;
            }
        }

        public Card[] Cards
        {
            get
            {
                return cards;
            }
        }

        public Card nextCard()
        {
            pos++;
            return cards[pos];
        }

        public void Shuffle()
        {
            Random r = new Random();

            for (int i = cards.Length - 1; i > 0; i--)
            {
                int p = r.Next(i + 1);
                Card temp = cards[i];
                cards[i] = cards[p];
                cards[p] = temp;

                // Console.WriteLine("{0} exchanged with {1}", i, p);
            }

            pos = -1;
        }
    }
}
