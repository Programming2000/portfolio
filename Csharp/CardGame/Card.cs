﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    class Card
    {
        protected Suits suit;
        protected string value;
        protected int number;

        public Card()
        {
        }
        public Card(Suits suit2, string value2, int number2)
        {
            suit = suit2;
            value = value2;
            number = number2;
        }

        public int Number
        {
            get { return number; }
        }
        public override string ToString()
        {
            return string.Format("{0} of {1}", value, suit);
        }
    }
}
