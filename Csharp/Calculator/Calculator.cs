// Calculator.cs
// Thomas Nilson
//
// TF072098

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace project
{
    /// <summary>
    /// Se Calculate.cs for information about the evaluate.dll.
    /// Handles communication with the form.
    /// </summary>
    public partial class Calculator : Form
    {
        /// <summary>
        /// 
        /// </summary>
        private String[] texts;
        private int counter;
        private String[] errorMessages = { "not a number after decimal point",
            "unrecognized symbol", "left parenthesis missing",
            "number of left and right parenthesises doesn't match",
            "operator expected", "number missing", "number missing",
            "operator expected", "division by zero" };

        /// <summary>
        /// Initializes components on the form.
        /// Creates a string array for the listbox so that printed expressions
        /// and results can be retrieved.
        /// Sets number of stored outputs to the listbox to zero.
        /// </summary>
        public Calculator()
        {
            InitializeComponent();
            texts = new String[1000];
            counter = 0;
        }

        /// <summary>
        /// Stores expression for later and shows it in the listbox.
        /// Calles handleNewInput method with the expression.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            String expression;
            String toTextbox;

            if (counter > 990)
            {
                listBoxResults.Items.Clear();
                counter = 0;
            }

            expression = textBoxExpression.Text;

            if (expression != "")
            {
                texts[counter] = expression;
                counter++;

                toTextbox = String.Format("{0}#  {1}", counter , expression);
                listBoxResults.Items.Add(toTextbox);
            
                handleNewInput(expression);
            }
        }

        /// <summary>
        /// Error codes:
        /// 1: not a number after decimal point 
        /// 2: unrecognized symbol
        /// 3: left parenthesis missing
        /// 4: number of left and right parenthesises doesn't match
        /// 5: operator expected
        /// 6: number missing
        /// 7: number missing
        /// 8: operator expected
        /// 9: division by zero
        /// </summary>
 
        /// <summary>
        /// Calls Interpret() in Calculate.cs.
        /// If there is no error.
        ///   the result is stored
        ///   the result is shown
        ///   the row with the result is highlighted
        ///   the textbox is cleared.
        /// If there is an error.
        ///   a empty string is stored as output
        ///   the corresponding errorcode is shown
        ///   the row with the errorcode is highlighted
        /// </summary>
        /// <param name="data"></param>
        private void handleNewInput(String data)
        {
            double res;
            String result;
            String toTextbox;
            int errorcode;

            res = Calculate.Interpret(data, out errorcode);

            if (errorcode == 0)
            {
                result = String.Format("{0}", res);
                texts[counter] = result;
                counter++;
                toTextbox = String.Format("{0}#  {1}", counter, result);
                listBoxResults.Items.Add(toTextbox);
                listBoxResults.SelectedIndex = counter - 1;
                textBoxExpression.Text = "";
            }
            else if ((errorcode >= 1) && (errorcode <= 9))
            {
                texts[counter] = "";
                counter++;
                toTextbox = String.Format("{0}#  {1}", counter,
                    errorMessages[errorcode-1]);
                listBoxResults.Items.Add(toTextbox);
                listBoxResults.SelectedIndex = counter - 1;
            }
        }

        /// <summary>
        /// Shows data from the listbox (output box) in the textbox (input box).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBoxResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selected;

            selected = listBoxResults.SelectedIndex;
            if ((selected >= 0) && (selected < counter))
                textBoxExpression.Text = texts[selected];
        }

        /// <summary>
        /// Clears textbox and sets counter for stored text to zero.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClearBox_Click(object sender, EventArgs e)
        {
            listBoxResults.Items.Clear();
            counter = 0;
        }

        /// <summary>
        /// Help text as a message box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonHelp_Click(object sender, EventArgs e)
        {
            String helpText = "Press 'Enter' or click 'calculate'-button to calculate."+
                "\nNo '=' at the end."+
                "\nYou can use decimal numbers with point or comma."+
                "\nYou can use the operators +,-,*,/ and ^."+
                "\nYou can use multiple '(' and ')'."+
                "\n'number(' is transformed to 'number*('."+
                "\nYou can write in rpn format (reverse polish format) if you"+
                "\nseparate positive numbers and operators with a 'space': 1 2 +. "+
                "\n"+
                "\nIf there is an error an errormessage will show in the output box"+
                "\nand the expression will be left in the input box to be corrected."+
                "\n"+
                "\nClick 'clear box'-button to clear the output textbox."+
                "\nYou can select previous expressions and results in the output box"+
                "\nand they will show in the input box."+
                "\nWhen an expression can be calculated the input box is cleared."+
                "\nThe program window can be resized.";

            MessageBox.Show(helpText, "Help text", MessageBoxButtons.OK);
        }

        /// <summary>
        /// Shows about box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAbout_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Calculator\nprogrammed by Thomas Nilson", "About",
                MessageBoxButtons.OK);

        }
    }
}