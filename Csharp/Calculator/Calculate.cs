// Calculate.cs
// Thomas Nilson
//
// TF072098

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace project
{
    /// <summary>
    /// Connection to a dll I programmed in c++.
    /// The original program was a project I did in a C programmings course
    /// at Malm� H�gskola Sweden the autumn of 2007. It was a console calculator.
    /// I have now compiled it in visual c++ 2005 express edition and made a
    /// dll-file from the functions in the program.
    /// </summary>
    class Calculate
    {
        /// <summary>
        /// dll-connection
        /// Char * in c++ becomes [MarshalAs(UnmanagedType.LPStr)] StringBuilder in c#.
        /// I found it easier to use that a byte in c# has the same length as char
        /// in c++.
        /// </summary>
        /// <param name="intext">user expression in byte=char format</param>
        /// <param name="a">a variable thats gets errorcode</param>
        /// <returns></returns>
        [DllImport("calculate.dll", EntryPoint = "evaluate",
        CallingConvention = CallingConvention.Cdecl)]
        static extern double evaluate(byte[] intext, out int a);
        
        /// <summary>
        /// Transfers string data to byte format to match char-type in c++.
        /// Adds '\0' witch indicates end of string in char pointer format.
        /// Calls evaluate function in calculate.dll to get result and an
        /// errorcode of 0.
        /// If it is not posssible to calculate a result an errorcode is returned.
        /// </summary>
        /// <param name="data">input expression</param>
        /// <param name="errorcode">an integer indicating why there is no result</param>
        /// <returns>calculated result</returns>
        public static double Interpret(String data, out int errorcode)
        {
            double numbers;
            byte[] data2 = new byte[100];
            int i;
            int size;

            size = data.Length;
           
            for (i = 0; i < size; i++)
            {
                data2[i] = (byte)data[i];
            }
            data2[size] = (byte)'\0';

            numbers = evaluate(data2, out errorcode);

            return numbers;
        }
    }
}
