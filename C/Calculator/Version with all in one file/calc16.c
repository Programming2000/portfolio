/*
calculator

Thomas Nilson

TF072098
*/

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <math.h>

/* inputs expression and stores it in char in[100]
   with '\0' as the last character */
void input(char in[]);

/* evaluate string with expression and return value
   expression in in[], stores calculated value in *result */
int evaluate(char in[],float *result);

/* store numbers, operators and parenthesises as codes
   in []: input expression
   *code1: symbols and numbers translated to codes
   *code2: numbers with codes 20 and up from *code1 stored in float format
   *count8: counter for *code1
   *count9: counter for *code2
   symbols and numbers stored as:
   + 1
   - 2
   * 3
   / 4
   ^ 5
   ( 18
   ) 19
   numbers stored as 20 and up */
int interpret(char in[],int *code1,float *code2,int *count4,int *count5);

/* read an number from in[] from position count1 and forward and return it
   in number[] with length *count3 */
int read_number(char in[],int count1,char number[],int *count3);

/* if the char in next is a digit return 1, if it's a '.' or ',' return 2 */
int check1(char next);

/* Translate the read and coded string to reverse polish notation (rpn)
   Rules:
   * Numbers pushed to the output when encountered (digits in the same order).
   * When an operator, if there is a operator with a higher precendence 
     at the top of the operator stack pop it and push it to the output.
     In all cases push the operator to the operator stack.
   * If a left parenthesis is found push it to the operator stack.
   * If a right parenthesis is found pop the operator stack and push
     the operators to the output until a left parenthesis is found.
   * When there are nothing more to be read in the input, push everything
     left on the operatorstack to the output.
   *code1: list of codes
   *code2: list of numbers
   count4: counter to *code1
   count5: counter to *code2
   *rpncode1: output, codes from *code1 in rpnformat */
int to_rpn(int *code1,float *code2,int count4,int count5,int *rpncode1);

/* calculate rpn list in the stack *rpncode1 from function to_rpn()
   numbers in *code2 and result in *result
   *rpncode1 in stack format with the start of the stack at *(rpncode1+1)
   and rpncode+*(rpncode) pointing to the top of the stack
   *code2 has all the numbers from the input string in order and in float
   format starting with *(code2) at code2 */
int calculate_rpn(int *rpncode1,float *code2,float *result);

/* zero stack counter at *(stack) and empty stack
   stack is at stack to stack+size-1
   and has the values *(stack+1) to *(stack+size-1) */
void init_stack(int *stack,int size);

/* stack push function
   gets stack counter at stack[0]=*stack=*(stack)
   increases stack counter and stores number
   at *(stack+counter) */
void push(int *stack,int number);

/* stack pop function
   gets the number at the top of the stack and decreases the stack counter */
void pop(int *stack, int *number);

/* gets the number at the top of the stack and returns it */
int top_of_stack(int *stack);

/* for printing */
const char symbol[6]={' ','+','-','*','/','^'};

/* Writes help text. Reads expression and evaluates it in a loop which can
   be exited by the user. 
   Error codes:
   1: not a number after decimal point 
   2: unrecognized symbol
   3: left parenthesis missing
   4: number of left and right parenthesises doesn't match
   5: operator expected
   6: number missing
   7: number missing
   8: operator expected */
int main(int argc, char *argv[])
{
     char in[100];
     double number;
     float number2;
     int b;
     float result;
     
     /* help text */
     printf("\n> Input calculator statement.");
     printf("\n> You can use floating point numbers with '.' or ','.");
     printf("\n> Use symbols +,-,*,/,^,( and ).");
     printf("\n> You can write expression in reverse polish notation.");
     printf("\n> Write 'exit' when finished.");
     /* input and evaluation loop */
     do
     {
          b=0;
          input(in);
          /* exit from program */
          if ((strcmp("exit",in)==0) || (strcmp("Exit",in)==0))
          {
               printf("\n");
               system("PAUSE");
               return 0;
          }
          /* calculate expression */	             
          b=evaluate(in,&result);
          /* if not error print result */
          
          if (b==1) printf("> not a number after decimal point");        
          if (b==2) printf("> unrecognized symbol");          
          if (b==3) printf("> left parenthesis missing");
          if (b==4) printf("> number of left and right parenthesises doesn't match");
          if (b==5) printf("\n> operator expected");
          if (b==6) printf("\n> number missing");
          if (b==7) printf("\n> number missing");
          if (b==8) printf("\n> number expected");

          if (b>0) printf("\n> error");
          else printf("\n> result: %g",result);          
     }
     while (1);

     return 0;
}

/* inputs expression and stores it in char in[100]
   with '\0' as the last character */
void input(char in[])
{
     printf("\n> ");
     /* flush input buffer */
     fflush(stdin);
     /* input string to in[] (scanf("%s",in); or fgets(in,99,stdin);) */
     gets(in);
}

/* evaluate string with expression and return value
   expression in in[], stores calculated value in *result */
int evaluate(char in[],float *result)
{
/*   code1: the expression interpreted with codes */
     int code1[50];
/*   code2: storing the read numbers in float format */
     float code2[50];
/*   rpn_code1: the codes from code1 in rpn order */
     int rpn_code1[50];
/*   count6: number of values stored in code1 */
     int count6=0;
/*   count7: number of values stored in code2 */
     int count7=0;
/*   a: error code */
     int a=0;
     
     *result=0;
     /* string to integer codes and numbers to float */
     /* gives error codes 1 and 2 */
     a=interpret(in,&code1[0],&code2[0],&count6,&count7);
     /* if error return */
     if (a>0) return a;      

     /* numbers and operators in rpn format */
     /* gives error codes 3 and 4 */
     a=to_rpn(&code1[0],&code2[0],count6,count7,&rpn_code1[0]);
     /* if error return */
     if (a>0) return a;     

     /* gives error codes 5,6,7 and 8 */
     /* evaluate rpn expression */
     a=calculate_rpn(&rpn_code1[0],&code2[0],result);
     /* if error return */
     if (a>0) return a;     

     return 0;     
}

/* store numbers, operators and parenthesises as codes
   in []: input expression
   *code1: symbols and numbers translared to codes
   *code2: numbers with codes 20 and up from *code1 stored in float format
   *count8: counter for *code1
   *count9: counter for *code2
   symbols and numbers stored as:
   + 1
   - 2
   * 3
   / 4
   ^ 5
   ( 18
   ) 19
   numbers stored as 20 and up */
int interpret(char in[],int *code1,float *code2,int *count8,int *count9)
{
     /* counter for position in char in[] */
     int count1=0;
     /* return value for length of read number */
     int count3=0;
     /* counter for *code1 */
     int count4=0;
     /* counter for *code2 */
     int count5=0;
     /* the code to store */
     int to_code1=0;
     /* current character in input string */
     char next;
     /* the read number if a digit is encountered */
     char number[20];
     /* the returned error value */
     int number_check;
     /* length of inout string */
     int length=0;
     /* from char number[] to double with function atof() */
     double do_number=0;
     
     /* length of statement */
     while (in[length]!='\0') length++;
     /* empty string */
     number[0]='\0';
     while (count1<length)
     {
          to_code1=0;
          next=in[count1];
          /* if a digit, read number, store number and update counter */
          if ((next-'0')>=0 && (next-'9')<=0)
               { 
                    number_check=read_number(in,count1,number,&count3);
                    if (number_check>0) return 1;
                    count1=count1+count3-1;
                    do_number=0;
                    do_number=atof(number);
                    /* expression starts with negative number */
                    if ((count4==1) && (*(code1)==2))
                    {
                         do_number=-do_number;     
                         count4--;
                    }
                    /* "(-number" */
                    if ((count4>1) && (*(code1+count4-1)==2)
                       && (*(code1+count4-2)==18))
                    {
                         do_number=-do_number;
                         count4--;
                         /* "(-number)" => "-number" */
                         if (in[count1+1]==')')
                         {
                              count1++;
                              count4--;
                         }
                    }
                    /* *code2[count5] */
                    *(code2+count5)=(float)do_number;
                    number[0]='\0';
                    count3=0;
                    to_code1=20+count5;
                    count5++;
                }
          /* translate symbols to integer codes */
          switch (next)
          {
               case '+':
                    to_code1=1;
                    break;
               case '-':
                    to_code1=2;
                    break;
               case '*':
                    to_code1=3;
                    break;
               case '/':
                    to_code1=4;
                    break;
               case '(': 
                    to_code1=18;
                    /* if 'number(' insert * so it's 'number*(' */
                    if ((*(code1+count4-1)>=20) && (count1>0))
                    {
                         *(code1+count4)=3;
                         count4++;
                    }
                    /* expression starts with "-(", replace with "-1*(" */
                    if ((count1==1) && *(code1)==2)
                    {
                         *(code1)=20;
                         *(code2)=-1;
                         count5=1;
                         *(code1+1)=3;
                         count4++;
                    }
                    break;
               case ')':
                    to_code1=19;
                    break;
               case '^':
                    to_code1=5;
                    break;
               /* ignore spaces */
               case ' ':
                    to_code1=100;
                    break;
          }
          /* unrecognized symbol */
          if (to_code1==0) return 2;
          /* store code if the character wasn't a space */
          if (to_code1!=100)
          {
               *(code1+count4)=to_code1;
               count4++;
          }
          /* increase position counter */
          count1++;
     }
     /* return counter values */
     *count8=count4;
     *count9=count5;               
     return 0;
}

/* read an number from in[] from position count1 and forward and return it
   in number[] with length *count3 */
int read_number(char in[],int count1,char number[],int *count3)
{
     int check=0;
     int count2=0;
     
     /* read while digits */
     while (check1(in[count1])==1)
     {
          number[count2]=in[count1];
          count1++;
          count2++;
     }
     check=check1(in[count1]);
     /* no decimal point, finish number with '\0', store counter and return */     
     if (check==0)
     {
          number[count2]='\0';
          *count3=count2;
          return 0;
     }
     /* check must have been ==2, decimal point */
     number[count2]='.';
     count2++;
     count1++;
     /* error code, no digits after decimal point */
     if (check1(in[count1])!=1) return 1;
     /* read decimals */
     while (check1(in[count1])==1)
     {
          number[count2]=in[count1];
          count1++;
          count2++;
     }
     /* finish number with '\0', store counter and return */
     number[count2]='\0';
     *count3=count2;
     return 0;
}

/* if the char in next is a digit return 1, if it's a '.' or ',' return 2 
   else return 0 */
int check1(char next)
{
     if ((next-'0')>=0 && (next-'9')<=0) return 1;
     if ((next=='.') || (next==',')) return 2;
     return 0;
}

/* Translate the read and coded string to reverse polish notation (rpn)
   Rules:
   * Numbers pushed to the output when encountered (digits in the same order).
   * When an operator, while there is a operator with a higher precendence 
     at the top of the operator stack pop it and push it to the output.
     In all cases push the operator to the operator stack.
   * If a left parenthesis is found push it to the operator stack.
   * If a right parenthesis is found pop the operator stack and push
     the operators to the output until a left parenthesis is found.
   * When there are nothing more to be read in the input, push everything
     left on the operatorstack to the output.
   *code1: list of codes
   *code2: list of numbers
   count4: counter to *code1
   count5: counter to *code2
   *rpncode1: output stack, codes from *code1 in rpnformat */
int to_rpn(int *code1,float *code2,int count4,int count5,int *rpncode1)
{
     /* loop counter for *code1 */
     int loop1;
     /* value from *code1 at place loop1 */
     int data1;
     /* operator stack */
     int stack1[50];
     /* top of operator stack */
     int out;
     /* size of operator stack */
     int number_of_rpnsymbols;
     /* value in output stack with counter loop1 */
     int rpn_symbol;
     
     /* init stack *rpncode1 */
     *(rpncode1)=0;
     /* there are zero numbers in the stack stack1[0]=0; */
     init_stack(&stack1[0],50);
     for (loop1=0; loop1<count4; loop1++)
     {
          data1=*(code1+loop1);
          
          /* if it's a number push it to the output at rpncode1 */
          if (data1>=20) push(rpncode1,data1);
          
          /* + or - */
          if ((data1==1) || (data1==2))
          {
               /* out=stack1[stack1[0]]; */                         
               out=top_of_stack(&stack1[0]);
               
               /* while the same priority or higher on top of stack */
               while ((out>=1) && (out<=5))
               {
                    pop(&stack1[0],&out);
                    push(rpncode1,out);
                    
                    out=top_of_stack(&stack1[0]);
               }
               
               push(&stack1[0],data1);
          }
          
          /* * or / */
          if ((data1==3) || (data1==4))
          {
               /* out=stack1[stack1[0]]; */                         
               out=top_of_stack(&stack1[0]);
               
               /* while the same priority or higher on top of stack */
               while ((out==3) || (out==4) || (out==5))
               {
                    pop(&stack1[0],&out);
                    push(rpncode1,out);
                    
                    out=top_of_stack(&stack1[0]);
               }
               
               push(&stack1[0],data1);               
          }

          /* ^ */
          /* ^ is right-associative in the shunting-yard algorithm */
          if (data1==5) push(&stack1[0],data1);

          /* put a left parenthesis at the operator stack */
          if (data1==18) push(&stack1[0],data1);
          
          /* right parenthesis, pop the operator stack until a left parenthesis
          */
          if (data1==19)
          {
               pop(&stack1[0],&out);
               do
               {
                    push(rpncode1,out);
                    pop(&stack1[0],&out);
               } 
               while ((out!=18) && (out!=0));

               if (out==0) return 3;
          }
     }
     
     /* empty stack with operators */
     pop(&stack1[0],&out);
     while (out>0)
     {
          /* left parenthesis on the stack */
          if ((out==18) || (out==19)) return 4;
          push(rpncode1,out);
          pop(&stack1[0],&out);
     }
     
     /* print rpn */
     number_of_rpnsymbols=*(rpncode1);
     printf("> rpn: ");
     for (loop1=0; loop1<number_of_rpnsymbols; loop1++)
     {
          rpn_symbol=*(rpncode1+loop1+1);
          /* operator +,-,*,/ or ^ */
          if ((rpn_symbol>=1) && (rpn_symbol<=5))
          {
               printf("%c ",symbol[rpn_symbol]);
          }
          /* ( */
          if (rpn_symbol==18) printf("( ");
          /* ) */
          if (rpn_symbol==19) printf(") ");
          /* if number print it with eventual decimals */
          if (rpn_symbol>=20)
          {
               printf("%g ",*(code2+rpn_symbol-20));
          }
     }
     return 0;
}

/* calculate rpn list in the stack *rpncode1 from function to_rpn()
   numbers in *code2 and result in *result
   *rpncode1 in stack format with the start of the stack at *(rpncode1+1)
   and rpncode+*(rpncode) pointing to the top of the stack
   *code2 has all the numbers from the input string in order and in float
   format starting with *(code2) at code2 */
int calculate_rpn(int *rpncode1,float *code2,float *result)
{
     /* calculation stack */
     int stack2[50];
     /* counter for the rpn list *rpncode1 */
     int count1;
     /* code from *rpncode1 */    
     int out;
     /* code for number a */
     int vara;
     /* code for number b */
     int varb;
     /* number a */
     float valuea;
     /* number b */
     float valueb;
     /* number of codes in rpncode1 */
     int length;
     
     *result=0;
     /* set stack counter to zero and write zeron in the total stack */
     init_stack(&stack2[0],50);     
     count1=0;
     length=*(rpncode1);
  
     /* only one number, the result is already finished */ 
     if ((length==1) && (*(rpncode1+1)==20))
     {
          *result=*(code2);
          return 0;
     }
     
     while (count1<length)
     {
          /* store numbers in the calculation stack until an operator
             is found */
          do
          {
               count1++;
               out=*(rpncode1+count1);
               push(&stack2[0],out);
          } 
          while ((out>=20) && (count1<length));
          /* pop operator */
          pop(&stack2[0],&out);
          if ((out<1) || (out>5)) return 5;
          /* if there is no number left to pop return with error */
          if (stack2[0]==0) return 6;
          /* pop code for number b */
          pop(&stack2[0],&varb);
          /* if there is no number left to pop return with error */          
          if (stack2[0]==0) return 7;
          /* pop code for number a */
          pop(&stack2[0],&vara);
          /* if a or b are not numbers return with error */
          if ((vara<20) || (varb<20)) return 8;
          /* numbers a and b in code2 */
          valuea=*(code2+vara-20);
          valueb=*(code2+varb-20);
          /* calculate according to operator */
          switch (out)
          {
               case 1:*result=valuea+valueb;
               break;
               case 2:*result=valuea-valueb;
               break;
               case 3:*result=valuea*valueb;
               break;
               case 4:*result=valuea/valueb;
               break;
               case 5:*result=powf(valuea,valueb);
               break;
          }
          push(&stack2[0],vara);
          *(code2+vara-20)=*result;
     }
     return 0;
}

/* zero stack counter at *(stack) and empty stack
   stack is at stack to stack+size-1
   and has the values *(stack+1) to *(stack+size-1) */
void init_stack(int *stack,int size)
{
     /* loop counter for *(stack), stack[loop1] */
     int loop1;

     for (loop1=0; loop1<size; loop1++) *(stack+loop1)=0;
}

/* stack push function
   gets stack counter at stack[0]=*stack=*(stack)
   increases stack counter and stores number
   at *(stack+counter) */
void push(int *stack,int number)
{
     /* stack counter */
     int a1;
     
     // a1=stack[0];
     a1=*(stack);
     /* increase stack counter */
     a1++;
     /* store number */
     // stack[a1]=number;
     *(stack+a1)=number;
     /* store stack counter */
     // stack[0]=a1;
     *(stack)=a1;
}

/* stack pop function
   gets the number at the top of the stack and decreases the stack counter */
void pop(int *stack, int *number)
{
     /* stack counter */
     int a1;
     
     *number=0;
     // a1=stack[0];
     a1=*(stack);
     /* if the stack has values get one */
     if (a1>0)
     {
          /* get value from stack */
          // *number=stack[a1];
          *number=*(stack+a1);
          /* decrease stack counter */
          a1--;
          // stack[0]=a1;
          *(stack)=a1;
     }
}

/* gets the number at the top of the stack and returns it */
int top_of_stack(int *stack)
{
     // return stack[stack[0]];
     return *(stack+*(stack));
}
