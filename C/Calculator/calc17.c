/*
calculator

Thomas Nilson

TF072098
*/

#include <stdio.h>
#include <stdlib.h>
/* int strcmp(const char* cs, const char* ct); 
   Compares cs with ct, returning negative value if cs<ct, zero if cs==ct, positive value if cs>ct. */
#include <strings.h>
#include "eval.h"

/* inputs expression and stores it in char in[100]
   with '\0' as the last character */
void input(char in[]);

/* Writes help text. Reads expression and evaluates it in a loop which can
   be exited by the user. 
   Error codes:
   1: not a number after decimal point 
   2: unrecognized symbol
   3: left parenthesis missing
   4: number of left and right parenthesises doesn't match
   5: operator expected
   6: number missing
   7: number missing
   8: operator expected */
int main(int argc, char *argv[])
{
     char in[100];
     double number;
     float number2;
     int b;
     float result;
     char err2[55];
     
     /* help text */
     printf("\n> Input calculator statement.");
     printf("\n> You can use floating point numbers with '.' or ','.");
     printf("\n> Use symbols +,-,*,/,^,( and ).");
     printf("\n> You can write expression in reverse polish notation.");
     printf("\n> Write 'exit' when finished.");
     /* input and evaluation loop */
     do
     {
          b=0;
          input(in);
          /* exit from program */
          if ((strcmp("exit",in)==0) || (strcmp("Exit",in)==0))
          {
               printf("\n");
               system("PAUSE");
               return 0;
          }
          /* calculate expression */	             
          b=evaluate(in,&result,1);

          /* if not error print result */
          if (b>0)
          {
               error(b,err2);
               printf("\n> %s",err2);
          }

          if (b>0) printf("\n> error");
          else printf("\n> result: %g",result);          
     }
     while (1);

     return 0;
}

/* inputs expression and stores it in char in[100]
   with '\0' as the last character */
void input(char in[])
{
     printf("\n> ");
     /* flush input buffer */
     fflush(stdin);
     /* input string to in[] (scanf("%s",in); or fgets(in,99,stdin);) */
     gets(in);
}

