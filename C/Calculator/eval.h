
#ifndef EVAL
#define EVAL

/* Error codes:
   1: not a number after decimal point 
   2: unrecognized symbol
   3: left parenthesis missing
   4: number of left and right parenthesises doesn't match
   5: operator expected
   6: number missing
   7: number missing
   8: operator expected */
void error(int code, char txt[]);

/* evaluate string with expression and return value
   expression in in[], stores calculated value in *result */
int evaluate(char in[],float *result,int outrpn);

/* store numbers, operators and parenthesises as codes
   in []: input expression
   *code1: symbols and numbers translared to codes
   *code2: numbers with codes 20 and up from *code1 stored in float format
   *count8: counter for *code1
   *count9: counter for *code2
   symbols and numbers stored as:
   + 1
   - 2
   * 3
   / 4
   ^ 5
   ( 18
   ) 19
   numbers stored as 20 and up */
int interpret(char in[],int *code1,float *code2,int *count4,int *count5);

/* read an number from in[] from position count1 and forward and return it
   in number[] with length *count3 */
int read_number(char in[],int count1,char number[],int *count3);

/* if the char in next is a digit return 1, if it's a '.' or ',' return 2 */
int check1(char next);

/* Translate the read and coded string to reverse polish notation (rpn)
   Rules:
   * Numbers pushed to the output when encountered (digits in the same order).
   * When an operator, if there is a operator with a higher precendence 
     at the top of the operator stack pop it and push it to the output.
     In all cases push the operator to the operator stack.
   * If a left parenthesis is found push it to the operator stack.
   * If a right parenthesis is found pop the operator stack and push
     the operators to the output until a left parenthesis is found.
   * When there are nothing more to be read in the input, push everything
     left on the operatorstack to the output.
   *code1: list of codes
   *code2: list of numbers
   count4: counter to *code1
   count5: counter to *code2
   *rpncode1: output, codes from *code1 in rpnformat */
int to_rpn(int *code1,float *code2,int count4,int count5,int *rpncode1);

void print_rpn(float *code2,int *rpncode1);

/* calculate rpn list in the stack *rpncode1 from function to_rpn()
   numbers in *code2 and result in *result
   *rpncode1 in stack format with the start of the stack at *(rpncode1+1)
   and rpncode+*(rpncode) pointing to the top of the stack
   *code2 has all the numbers from the input string in order and in float
   format starting with *(code2) at code2 */
int calculate_rpn(int *rpncode1,float *code2,float *result);

/* zero stack counter at *(stack) and empty stack
   stack is at stack to stack+size-1
   and has the values *(stack+1) to *(stack+size-1) */
void init_stack(int *stack,int size);

/* stack push function
   gets stack counter at stack[0]=*stack=*(stack)
   increases stack counter and stores number
   at *(stack+counter) */
void push(int *stack,int number);

/* stack pop function
   gets the number at the top of the stack and decreases the stack counter */
void pop(int *stack, int *number);

/* gets the number at the top of the stack and returns it */
int top_of_stack(int *stack);

#endif
